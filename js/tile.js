'use strict';

class Tile {
	constructor(_globe, _tileX, _tileY, _zoom) {
		this.evt = new Evt();
		this.globe = _globe;
		this.tileX = _tileX;
		this.tileY = _tileY;
		this.zoom = _zoom;
		this.visible = false;
		this.meshe = null;
		this.tileDefinition = 1;
		const coordStart = App.GeoTools.tileToCoord(this.tileX, this.tileY, this.zoom);
		const coordEnd = App.GeoTools.tileToCoord(this.tileX + 1, this.tileY + 1, this.zoom);
		this.bbox = new App.GeoTools.BoundingBox(coordStart[0], coordStart[1], coordEnd[0], coordEnd[1]);
		this.parentTile = null;
		this.childsTiles = [];
		this.elevation = null;
		this.defaultMap = App.Net.Texture.get('loading');
		this.material = new THREE.MeshStandardMaterial({color:0xffffff,map:this.defaultMap,roughness:1,metalness:0,transparent:true,});
		this.uvOffset = {
			factor : 1, 
			x : 0, 
			y : 0, 
		};
		this.disposed = false;
	}
	
	onChildChange(_evt) {
		this.evt.fireEvent('CHANGE', _evt);
	}
	
	displayZoomAtCoord(_zoom, _coord, _marginFactor) {
		if (!this.containsCoord(_coord, _marginFactor)) {
			this.merge();
			return false;
		}
		if (this.zoom == _zoom) return false;
		if (this.zoom > _zoom) {
			this.merge();
		} else if (this.zoom < _zoom) {
			this.divide();
			this.childsTiles.forEach(t => t.displayZoomAtCoord(_zoom, _coord, _marginFactor));
		}
		return true;
	}
	
	divide() {
		if (this.childsTiles.length > 0) return false;
		const childsOffset = [
			[0, 0], 
			[0, 1], 
			[1, 0], 
			[1, 1], 
		];
		let child;
		childsOffset.forEach(offset => {
			const childOffsetX = this.tileX * 2 + offset[0];
			const childOffsetY = this.tileY * 2 + offset[1];
			const childUvFactor = this.uvOffset.factor / 2;
			child = new Tile(this.globe, childOffsetX, childOffsetY, this.zoom + 1);
			child.evt.listen('CHANGE', this, this.onChildChange);
			child.parentTile = this;
			child.material.map = this.material.map;
			child.buildMeshe(this.tileDefinition);
			child.updateUvs(
				childUvFactor, 
				this.uvOffset.x + (childUvFactor * offset[0]), 
				this.uvOffset.y + (childUvFactor * offset[1])
			);
			child.isReady();
			child.show();
			this.childsTiles.push(child);
		});
		this.hide();
		this.evt.fireEvent('CHANGE', {event : 'DIVIDE', tile : this});
	}
	
	merge() {
		if (this.childsTiles.length == 0) return false;
		this.childsTiles.forEach(t => t.dispose());
		this.childsTiles = [];
		this.evt.fireEvent('CHANGE', {event : 'MERGE', tile : this});
		this.show();
	}
	
	show() {
		if (this.visible) return false;
		this.globe.meshe.add(this.meshe);
		this.visible = true;
		this.evt.fireEvent('CHANGE', {event : 'SHOW', tile : this});
		App.Renderer.mustRender();
	}
	
	hide() {
		if (!this.visible) return false;
		this.globe.meshe.remove(this.meshe);
		this.visible = false;
		this.evt.fireEvent('CHANGE', {event : 'HIDE', tile : this});
		App.Renderer.mustRender();
	}
	
	containsCoord(_coord, _marginFactor) {
		let bbox = this.bbox;
		if (_marginFactor != 1) bbox = this.bbox.scale(_marginFactor);
		return bbox.containsCoord(_coord);
	}
	
	intersect(_bbox) {
		return this.bbox.containsBbox(_bbox);
	}
	
	isReady() {
		this.evt.fireEvent('CHANGE', {event : 'READY', tile : this});
	}
	
	buildMesheBuffer(_definition) {
		this.tileDefinition = _definition;
		const vertBySide = this.tileDefinition + 1;
		let x, y;
		const nbFacesTri = Math.pow(this.tileDefinition, 2) * 2;
		const nbVertices = vertBySide * vertBySide;
		const bufferFaces = new Uint32Array(nbFacesTri * 3);
		const bufferVertices = new Float32Array(nbVertices * 3);
		const bufferUvs = new Float32Array(nbVertices * 2);
		// Faces
		let faceIndex = 0;
		for(x = 0; x < this.tileDefinition; x ++){
			for (y = 0; y < this.tileDefinition; y ++) {
				bufferFaces[faceIndex + 0] = (x * vertBySide) + y;
				bufferFaces[faceIndex + 1] = (x * vertBySide) + (y + 1);
				bufferFaces[faceIndex + 2] = ((x + 1) * vertBySide) + (y + 1);
				faceIndex += 3;
				bufferFaces[faceIndex + 0] = ((x + 1) * vertBySide) + (y + 1);
				bufferFaces[faceIndex + 1] = ((x + 1) * vertBySide) + y;
				bufferFaces[faceIndex + 2] = (x * vertBySide) + y;
				faceIndex += 3;
			}
		}
		// Vertices
		/*
		let vect;
		const coord = new App.GeoTools.Coord();
		const stepCoord = [
			this.bbox.width / this.tileDefinition, 
			this.bbox.height / this.tileDefinition, 
		];
		let verticeIndex = 0;
		for(x = 0; x < vertBySide; x ++){
			for (y = 0; y < vertBySide; y ++) {
				coord.set(
					this.bbox.startCoord.lon + (stepCoord[0] * x), 
					this.bbox.startCoord.lat + (stepCoord[1] * y), 
					this.elevationByVert(x, y)
				);
				vect = App.Globe.coordToVect(coord);
				bufferVertices[verticeIndex + 0] = vect.x;
				bufferVertices[verticeIndex + 1] = vect.y;
				bufferVertices[verticeIndex + 2] = vect.z;
				verticeIndex += 3;
			}
		}
		*/
		var bufferGeometry = new THREE.BufferGeometry();
		bufferGeometry.addAttribute('position', new THREE.BufferAttribute(bufferVertices, 3));
		bufferGeometry.addAttribute('uv', new THREE.BufferAttribute(bufferUvs, 2));
		bufferGeometry.setIndex(new THREE.BufferAttribute(bufferFaces, 1));
		this.meshe = new THREE.Mesh(bufferGeometry, this.material);
		// console.log('build');
		this.updateUvs();
		this.updateVerticesPos();
		this.meshe.geometry.computeFaceNormals();
		this.meshe.geometry.computeVertexNormals();
		this.meshe.geometry.computeBoundingSphere();
		this.meshe.geometry.computeBoundingBox();
		this.meshe.matrixAutoUpdate = false;
		this.evt.fireEvent('CHANGE', {event : 'BUILD_MESH', tile : this});
		return this.meshe;
	}
	
	buildMeshe(_definition) {
		return this.buildMesheBuffer(_definition);
		
		this.tileDefinition = _definition;
		const vertBySide = this.tileDefinition + 1;
		let x, y;
		const stepUV = 1 / this.tileDefinition;
		let geometry = new THREE.Geometry();
		for(x = 0; x < vertBySide; x ++){
			for (y = 0; y < vertBySide; y ++) {
				geometry.vertices.push(new THREE.Vector3());
			}
		}
		for (x = 0; x < this.tileDefinition; x ++) {
			for (y = 0; y < this.tileDefinition; y ++) {
				geometry.faces.push(new THREE.Face3((y + 1) + (x * vertBySide), y + ((x + 1) * vertBySide), y + (x * vertBySide)));
				geometry.faceVertexUvs[0][(geometry.faces.length - 1 )] = [new THREE.Vector2(), new THREE.Vector2(), new THREE.Vector2()];
				geometry.faces.push(new THREE.Face3((y + 1) + (x * vertBySide), (y + 1) + ((x + 1) * vertBySide), y + ((x + 1) * vertBySide)));
				geometry.faceVertexUvs[0][(geometry.faces.length - 1)] = [new THREE.Vector2(), new THREE.Vector2(), new THREE.Vector2()];
			}
		}
		this.meshe = new THREE.Mesh(geometry, this.material);
		this.updateUvs();
		this.updateVerticesPos();
		this.meshe.geometry.computeFaceNormals();
		this.meshe.geometry.computeVertexNormals();
		this.meshe.matrixAutoUpdate = false;
		this.evt.fireEvent('CHANGE', {event : 'BUILD_MESH', tile : this});
		return this.meshe;
	}
	
	updateUvs(_factor = 1, _offsetX = 0, _offsetY = 0) {
		// return false;
		this.uvOffset = {
			factor : _factor, 
			x : _offsetX, 
			y : _offsetY
		};
		// console.log('updateUvs', this.zoom, this.tileX, this.tileY, this.uvOffset);
		// console.log(this.uvOffset);
		// _offsetY *= -1;
		const uvStep = this.uvOffset.factor / this.tileDefinition;
		let uvIndex = 0;
		for (let x = 0; x < this.tileDefinition + 1; x ++) {
			for (let y = 0; y < this.tileDefinition + 1; y ++) {
				this.meshe.geometry.attributes.uv.array[uvIndex + 0] = this.uvOffset.x + (x * uvStep);
				this.meshe.geometry.attributes.uv.array[uvIndex + 1] = 1 - _offsetY - (y * uvStep);
				uvIndex += 2;
			}
		}
		// console.log(this.meshe.geometry.attributes.uv.array);
		// this.meshe.geometry.uvsNeedUpdate = true;
		this.meshe.geometry.attributes.uv.needsUpdate = true;
	}
	
	updateVerticesPos() {
		let x, y;
		let vect;
		const coord = new App.GeoTools.Coord();
		const stepCoord = [
			this.bbox.width / this.tileDefinition, 
			this.bbox.height / this.tileDefinition, 
		];
		let verticeIndex = 0;
		for(x = 0; x < this.tileDefinition + 1; x ++){
			for (y = 0; y < this.tileDefinition + 1; y ++) {
				coord.set(
					this.bbox.startCoord.lon + (stepCoord[0] * x), 
					this.bbox.startCoord.lat + (stepCoord[1] * y), 
					this.elevationByVert(x, y)
				);
				vect = App.Globe.coordToVect(coord);
				this.meshe.geometry.attributes.position.array[verticeIndex + 0] = vect.x;
				this.meshe.geometry.attributes.position.array[verticeIndex + 1] = vect.y;
				this.meshe.geometry.attributes.position.array[verticeIndex + 2] = vect.z;
				verticeIndex += 3;
			}
		}
		this.meshe.geometry.computeFaceNormals();
		this.meshe.geometry.computeVertexNormals();
		// this.meshe.geometry.verticesNeedUpdate = true;
		this.meshe.geometry.attributes.position.needsUpdate = true;
		this.evt.fireEvent('CHANGE', {event : 'UPDATE_VERTICES', tile : this});
		App.Renderer.mustRender();
		return false;
		
		/*
		let vect;
		let vectId = 0;
		const coord = new App.GeoTools.Coord();
		const vertBySide = this.tileDefinition + 1;
		const stepCoord = [
			this.bbox.width / this.tileDefinition, 
			this.bbox.height / this.tileDefinition, 
		];
		for(let x = 0; x < vertBySide; x ++){
			for (let y = 0; y < vertBySide; y ++) {
				coord.set(
					this.bbox.startCoord.lon + (stepCoord[0] * x), 
					this.bbox.startCoord.lat + (stepCoord[1] * y), 
					this.elevationByVert(x, y)
				);
				vect = App.Globe.coordToVect(coord);
				this.meshe.geometry.vertices[vectId].set(vect.x, vect.y, vect.z);
				vectId ++;
			}
		}
		this.meshe.geometry.computeFaceNormals();
		this.meshe.geometry.computeVertexNormals();
		this.meshe.geometry.verticesNeedUpdate = true;
		this.evt.fireEvent('CHANGE', {event : 'UPDATE_VERTICES', tile : this});
		App.Renderer.mustRender();
		*/
	}
	
	elevationAt(_coord) {
		if (!this.containsCoord(_coord, 1)) return false;
		let alt = this.childsTiles.map(c => c.elevationAt(_coord)).filter(ele => ele !== false).pop();
		if (alt != undefined) {
			return alt;
		}
		alt = this.calcCoordElevation(_coord);
		return alt;
	}
	
	calcCoordElevation(_coord) {
		const distLon = _coord.lon - this.bbox.startCoord.lon;
		const distLat = _coord.lat - this.bbox.startCoord.lat;
		const prctLon = distLon / this.bbox.width;
		const prctLat = distLat / this.bbox.height;
		const vertX = prctLon * this.tileDefinition;
		const vertY = prctLat * this.tileDefinition;
		const xA = Math.floor(vertX);
		const xB = Math.ceil(vertX);
		const yA = Math.floor(vertY);
		const yB = Math.ceil(vertY);
		const eleXA = this.elevationByVert(xA, yA);
		const eleXB = this.elevationByVert(xB, yA);
		const eleYA = this.elevationByVert(xA, yB);
		const eleYB = this.elevationByVert(xB, yB);
		const prctX = vertX - xA;
		const prctY = vertY - yA;
		return App.Math.interpolate(eleXA, eleXB, eleYA, eleYB, prctX, prctY);
	}
	
	elevationByVert(_x, _y) {
		if (!this.elevation) return 0;
		const index = _x * (this.tileDefinition + 1) + _y;
		return this.elevation[index];
	}
	
	dispose() {
		this.merge();
		if (this.meshe) {
			this.globe.meshe.remove(this.meshe);
			this.meshe.geometry.dispose();
			this.meshe.material.dispose();
			this.meshe = null;
		}
		this.disposed = true;
		this.evt.fireEvent('CHANGE', {event : 'DISPOSE', tile : this});
	}
}