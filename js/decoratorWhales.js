'use strict';

App.require('decorator.js');
App.require('actors/whale.js');


App.Decorator.Whales = class extends App.Decorator {
	constructor() {
		super()
		this.name = 'WHALES';
		this.decoratorSea = App.DecoratorRegister.invoke('SEA');
		this.tilesWhales = {};
		App.Globe.evt.listen('TILE_CHANGE-READY', this, this.onTileReady);
		App.DecoratorRegister.register(this);
	}
	
	onTileReady(_tile) {
		if (_tile.zoom < 12) return false;
		if (this.decoratorSea.isCoordOnGround(_tile.bbox.center())) return false;
		const randomGen = new alea(_tile.tileX + '' + _tile.tileY + '' + _tile.zoom);
		const random = randomGen();
		if (random > 0.1) return false;
		const tileKey = this.tileKey(_tile);
		this.tilesWhales[tileKey] = new Whale(_tile.bbox);
	}
	
	onTileDispose(_tile) {
		const tileKey = this.tileKey(_tile);
		if (this.tilesWhales[tileKey]) {
			this.tilesWhales[tileKey].dispose();
			delete this.tilesWhales[tileKey];
		}
		super.onTileDispose(_tile);
	}	
	
	dispose() {
		super.dispose();
		App.Globe.evt.removeEventListener('TILE_CHANGE-READY', this, this.onTileReady);
	}
}