'use strict';

App.require('decorator.js');

App.Decorator.Normal = class extends App.Decorator {
	constructor() {
		super()
		this.name = 'NORMAL';
		const loadersNb = 1;
		for (let i = 0; i < loadersNb; i ++) {
			const loader = new Tile2DProvider(
				'https://val.openearthview.net/libs/remoteImg.php?tileNormal=1&def=64', 			
				(_texture, _params) => this.onDatasLoaded(_texture, _params)
			);
			this.dataLoaders.push(loader);
		}
		App.Globe.evt.listen('TILE_CHANGE-READY', this, this.onTileBuildMeshe);
		App.DecoratorRegister.register(this);
	}
	
	onTileBuildMeshe(_tile) {
		if (_tile.zoom < 11) return false;
		const params = {
			x : _tile.tileX, 
			y : _tile.tileY, 
			z : _tile.zoom, 
		};
		if (!this.addToWaiting(_tile, params)) return false;
		this.prioritizeQueue();
		this.processNextLoading();
	}
	
	onDatasLoaded(_datas, _params) {
		const entry = super.onDatasLoaded(_datas, _params);
		if (!entry) return false;
		entry.tile.material.normalMap = _datas;
		entry.tile.material.needsUpdate = true;
		App.Renderer.mustRender();
		this.onDataProcessed(entry);
	}
	
	dispose() {
		super.dispose();
		App.Globe.evt.removeEventListener('TILE_CHANGE-READY', this, this.onTileBuildMeshe);
		this.tilesProcessed.forEach(entry => {
			entry.tile.material.map = entry.tile.defaultMap;
			entry.tile.material.needsUpdate = true;
		});
	}
}