'use strict';

App.require('geoTools.js');

var App = (function(_app) {
	
	let environmentMeshe;
	let lightSun;
	let lightAmbiant;
	let sunCoord;
	let debugSun;
	let mesheSky;
	let colorsGradient;
	let normalizedTime = 3;
	let grabMove = false;
	
	var api = {};
	
	api.build = function() {
		colorsGradient = getImageData(App.Net.Texture.get('skyColors').image);	
		environmentMeshe = new THREE.Mesh(new THREE.Geometry());
		App.Renderer.add(environmentMeshe);
		
		
		
		
		// const skyShader = App.Shader.get('grass')
		var vertexShaderSky = document.getElementById('vertex-sky').textContent;
		var fragmentShaderSky = document.getElementById('fragment-sky').textContent;
		var uniformsSky = {
			// cameraZoom : {value : 1}, 
			// sunPos : {value : new THREE.Vector3(0,0,0)}, 
		};
		var parametersSky = {
			fragmentShader: fragmentShaderSky,
			vertexShader: vertexShaderSky,
			uniforms: uniformsSky, 
			transparent: true,
			side : THREE.BackSide, 
		};
		var materialSky = new THREE.ShaderMaterial(parametersSky);
		const geoSky = new THREE.SphereGeometry(App.Globe.radius * 1.03, 64, 64);
		mesheSky = new THREE.Mesh(geoSky, materialSky);
		environmentMeshe.add(mesheSky);
		
		
		
		App.GeoCamera.evt.listen('ZOOM_CHANGED', api, api.onZoomChanged);
		buildLights();
		updateLights();
		api.updateLocalTime(0);
		App.Renderer.evt.listen('RENDER', api, api.onFrame);
		App.Input.Mouse.evt.listen('MOUSE_LEFT_DOWN', api, onMouseLeftDown);
		App.Input.Mouse.evt.listen('MOUSE_LEFT_UP', api, onMouseLeftUp);
	}
	
	api.onZoomChanged = function(_zoom) {
		const fogDensity = (App.Globe.radius / Math.pow(2, (_zoom - 4)));
		App.Renderer.setFogDensity(fogDensity, fogDensity * 10);
		
		
		// mesheSky.material.uniforms.cameraZoom.value = _zoom;
		// mesheSky.material.uniforms.sunPos.value = App.Globe.coordToVect(sunCoord);
	}
	
	function onMouseLeftDown() {
		const groundHit = App.Renderer.screenPositionHitObject(
			App.Input.Mouse.curMouseX, 
			App.Input.Mouse.curMouseY, 
			App.Globe.meshe
		);
		if (groundHit) return false;
		grabMove = true;
	}
	
	function onMouseLeftUp() {
		grabMove = false;
	}
	
	api.onFrame = function() {
		// sunCoord.lon += 0.5;
		// if (sunCoord.lon > 180) sunCoord.lon -= 360;
		// if (sunCoord.lon < -180) sunCoord.lon += 360;
		// updateLights();
		// App.Renderer.mustRender();
		if (grabMove) {
			grabSun();
		}
	}
	
	function grabSun() {
		const pos = App.Renderer.screenToContainer(App.Input.Mouse.curMouseX, App.Input.Mouse.curMouseY);
		// sunCoord.lon = pos.x * 180;
		sunCoord.lon = App.GeoTools.loopLon(App.GeoCamera.groundCoord.lon + pos.x * -180);
		// getTimeFromCamera();
		api.updateLocalTime(pos.x);
	}
	
	function getTimeFromCamera() {
		const distToCam = sunCoord.distanceTo(App.GeoCamera.groundCoord);
		const prct = distToCam / 40075016;
		let lonDiff = (App.GeoCamera.coord.lon - sunCoord.lon) / 180;
		lonDiff = parseFloat(lonDiff.toFixed(2));
		api.updateLocalTime(lonDiff);
	}
	
	api.updateLocalTime = function(_normalizedTime) {
		if (normalizedTime == _normalizedTime) return false;
		normalizedTime = _normalizedTime;
		const percent = (normalizedTime + 1) / 2;
		const skyColor = getRampColor(percent, 1);
		const sunColor = getRampColor(percent, 60);
		lightSun.color = sunColor;
		App.Renderer.setFogColor(skyColor);
		debugSun.material.color = sunColor;
		debugSun.material.needsUpdate = true;
		// mesheSky.material.color = skyColor;
		// mesheSky.material.needsUpdate = true;
		updateLights();
		App.Renderer.mustRender();
	}
	
	function getRampColor(_percent, _column) {
		const gradientValue = Math.round(_percent * 127);
		var rampColor = getPixel(colorsGradient, _column, gradientValue);
		return new THREE.Color('rgb(' + rampColor.r + ',' + rampColor.g + ',' + rampColor.b + ')');
	}
	
	function updateLights() {
		const sunVect = App.Globe.coordToVect(sunCoord);
		lightSun.position.set(sunVect.x, sunVect.y, sunVect.z);
		debugSun.position.set(sunVect.x, sunVect.y, sunVect.z);
	}
	
	function buildLights() {
		sunCoord = new App.GeoTools.Coord(3, 30, 1000000);
		lightSun = new THREE.DirectionalLight(0xffffff, 0.9);
		lightSun.target = App.Globe.meshe;
		App.Renderer.add(lightSun);
		// lightAmbiant = new THREE.AmbientLight(0x25282d);
		lightAmbiant = new THREE.AmbientLight(0x101214);
		environmentMeshe.add(lightAmbiant);
		let geometrySun = new THREE.SphereGeometry(10000, 16, 16);
		
		debugSun = new THREE.Mesh(geometrySun, new THREE.MeshBasicMaterial({color:0xffffee,fog:false}));
		environmentMeshe.add(debugSun);
	}
	
	function getImageData(image) {
		var canvas = document.createElement('canvas');
		canvas.width = image.width;
		canvas.height = image.height;
		var context = canvas.getContext('2d');
		context.drawImage(image, 0, 0);
		return context.getImageData(0, 0, image.width, image.height);
	}
	
	function getPixel(imagedata, x, y) {
		const position = (x + imagedata.width * y) * 4;
		return {
			r: imagedata.data[position], 
			g: imagedata.data[position + 1], 
			b: imagedata.data[position + 2], 
			a: imagedata.data[position + 3]
		};
	}
	
	_app.Sky = api;
	
	return _app;
})(App);

