'use strict';

var App = (function(_app) {
	var api = {
		init : function() {
			App.Input.Keyboard.init();
			App.Input.Mouse.init();
		}, 
		
	};
	
	_app.initOnStart(api);
	_app.Input = api;
	
	return _app;
})(App);


App.Input.Keyboard = (function() {
	var majActiv = false;
	var ctrlActiv = false;
	
	var api ={
		lastKeyDown : -1, 
		lastKeyUp : -1, 
		evt : new Evt(), 
		
		init : function() {
			document.body.addEventListener('keydown', api.onKeyDown);
			document.body.addEventListener('keyup', api.onKeyUp);
		}, 
		
		onKeyDown : function(event) {
			if (document.activeElement.type == undefined ){
				var key = event.keyCode || event.which;
				var keychar = String.fromCharCode(key);
				// console.log( "onKeyDown : " + key + " / " + keychar );
				if( api.lastKeyDown != key ){
					api.lastKeyUp = -1;
					api.lastKeyDown = key;
					api.evt.fireEvent("ON_KEY_DOWN");
					api.evt.fireEvent('ON_KEY_DOWN_' + key);
				}
				if( key == 87 ){ // w
					
				}else if( key == 16 ){ // MAJ
					majActiv = true;
				}else if( key == 17 ){ // CTRL
					ctrlActiv = true;
				}else if( key == 37 ){ // LEFT
					
				}else if( key == 39 ){ // RIGHT
					
				}else if( key == 38 ){ // TOP

				}else if( key == 40 ){ // BOTTOM

				}else if( key == 65 ){ // a
					
				}else if( key == 66 ){ // b
					
				}else if( key == 67 ){ // c
					
				}else if( key == 68 ){ // d
					
				}else if( key == 69 ){ // e

				}else if( key == 71 ){ // g
				
				}else if( key == 76 ){ // l

				}else if( key == 79 ){ // o
					
				}else if( key == 80 ){ // p
					
				}else if( key == 82 ){ // r
					
				}else if( key == 83 ){ // s
					
				}else if( key == 90 ){ // z
					
				}else if( key == 107 ){ // +
					
				}else if( key == 109 ){ // -
					
				}else if( key == 88 ){ // x
					
				}
			}
		}, 
				
		onKeyUp : function(evt) {
			var key = evt.keyCode || evt.which;
			var keychar = String.fromCharCode(key);
			if( api.lastKeyUp != key ){
				api.lastKeyDown = -1;
				api.lastKeyUp = key;
				api.evt.fireEvent("ON_KEY_UP");
			}
			if( key == 17 ){ // CTRL
				ctrlActiv = false;
			}else if( key == 16 ){ // MAJ
				majActiv = false;
			}
		}, 
	};
	
	return api;
})();

App.Input.Mouse = (function() {
	let lastWheelTime = 0;
	var api = {
		curMouseX : 0, 
		curMouseY : 0, 
		mouseBtnLeftState : false, 
		mouseBtnMiddleState : false, 
		mouseBtnRightState : false, 
		evt : null, 
		
		init : function() {
			api.evt = new Evt()
			document.onmousemove = api.onMouseMove;
			document.addEventListener('mousedown', onMouseDown);
			document.addEventListener('mouseup', onMouseUp);
			document.addEventListener('mousewheel', api.onMouseWheel, {passive: true});
			document.addEventListener('contextmenu', evt => evt.preventDefault());
		}, 
		
		onMouseMove : function(evt) {
			api.curMouseX = evt.clientX;
			api.curMouseY = evt.clientY;
		}, 
		
		onMouseWheel : function(evt) {
			const curTime = Date.now();
			if ((curTime - lastWheelTime) < 100) return false;
			lastWheelTime = curTime;
			// const delta = evt.wheelDelta > 1 ? 1 : -1;;
			const delta = evt.wheelDelta / 180;
			api.evt.fireEvent('MOUSE_WHEEL', delta);
		}, 
	};
	
	function onMouseDown(evt) {
		switch (evt.button) {
			case 0:
				api.mouseBtnLeftState = true;
				onMouseLeftDown();
			break;
			case 1:
			   api.mouseBtnMiddleState = true;
			break;
			case 2:
				api.mouseBtnRightState = true;
				onMouseRightDown();
			break;
		}
	}
	
	function onMouseUp(evt) {
		switch (evt.button) {
			case 0:
				api.mouseBtnLeftState = false;
				onMouseLeftUp();
			break;
			case 1:
				api.mouseBtnMiddleState = false;
			break;
			case 2:
				api.mouseBtnRightState = false;
				onMouseRightUp();
			break;
		}
	}
	
	function onMouseLeftDown() {
		api.evt.fireEvent('MOUSE_LEFT_DOWN');
	}
	function onMouseRightDown() {
		api.evt.fireEvent('MOUSE_RIGHT_DOWN');
	}
	
	function onMouseLeftUp() {
		api.evt.fireEvent('MOUSE_LEFT_UP');
	}
	function onMouseRightUp() {
		api.evt.fireEvent('MOUSE_RIGHT_UP');
	}
	
	return api;
})(App);
