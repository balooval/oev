'use strict';

App.require('decorator.js');
App.require('shader.js');
App.require('surfaceMaker.js');

App.Decorator.Grass = class extends App.Decorator {
	constructor() {
		super()
		this.name = 'SURFACE';
		// const loader = new FakeProvider((_params) => this.onDatasLoaded(_params));
		const loader = new App.Provider.Landuse((_json, _params) => this.onDatasLoaded(_json, _params));
		this.dataLoaders.push(loader);
		this.nbLayers = 15;
		this.layerThickness = App.Globe.toMeter(1.5);
		this.grassMaterial = this.prepareMaterial();
		App.Globe.evt.listen('TILE_CHANGE-READY', this, this.onTileReady);
		App.Globe.evt.listen('TILE_CHANGE-UPDATE_VERTICES', this, this.onTileUpdateVertices);
		App.DecoratorRegister.register(this);
	}
	
	prepareMaterial() {
		const grassShader = App.Shader.get('grass');
		const params = {
			vertexShader: grassShader.vertex,
			fragmentShader: grassShader.fragment, 
			uniforms: {
				time : {value : 0.0}, 
				nbLayers : {value : this.nbLayers}, 
				furMap : {type: 't', value: App.Net.Texture.get('fur')},
				maskMap : {type: 't', value: null},
			}, 
			transparent : true, 
			depthTest: false, 
			depthWrite: true, 
		};
		return new THREE.ShaderMaterial(params);
	}
	
	onTileReady(_tile) {
		if (_tile.zoom < 14) return false;
		// const params = {
			// x : _tile.tileX, 
			// y : _tile.tileY, 
			// z : _tile.zoom, 
		// };
		
		const params = App.GeoTools.tilePosAtZoom(
			_tile.tileX, 
			_tile.tileY, 
			_tile.zoom, 
			14
		);
		params.keyCache = params.x + '-' + params.y + '-' + params.z
		
		if (!this.addToWaiting(_tile, params)) return false;
		this.prioritizeQueue();
		this.processNextLoading();
	}
	
	onTileUpdateVertices(_tile) {
		const tileKey = this.tileKey(_tile);
		this.tilesProcessed.filter(e => e.key == tileKey).forEach(e => {
			// console.log('onTileUpdateVertices');
			const material = e.grassMeshe.material;
			e.grassMeshe.parent.remove(e.grassMeshe);
			e.grassMeshe.geometry.dispose();
			e.grassMeshe = null;
			this.buildGrassForEntry(e, material);
		});
	}
	
	onDatasLoaded(_json, _params) {
		const entry = super.onDatasLoaded(_json, _params);
		if (!entry) return false;
		const canvas = App.SurfaceMaker.construct(_json, entry.tile.bbox);
		const map = new THREE.Texture(canvas);
		map.needsUpdate = true;
		const repeatUv = Math.max(1, Math.pow(2, 16 - entry.tile.zoom));
		this.buildGrassForEntry(entry, this.buildMaterial(map, repeatUv));
		this.onDataProcessed(entry);
	}
	
	buildMaterial(_map, _repeat) {
		// return new THREE.MeshBasicMaterial({color:'#ffffff',map:_map,transparent:true});
		const grassShader = App.Shader.get('grass');
		const params = {
			vertexShader: grassShader.vertex,
			fragmentShader: grassShader.fragment, 
			uniforms: {
				time : {value : 0.0}, 
				nbLayers : {value : this.nbLayers}, 
				furMap : {type: 't', value: App.Net.Texture.get('fur')},
				maskMap : {type: 't', value: _map},
				repeat : {type: 't', value: _repeat},
			}, 
			transparent : true, 
			depthTest: false, 
			depthWrite: true, 
		};
		return new THREE.ShaderMaterial(params);
	}
	
	buildGrassForEntry(_entry, _material) {
		var geo = _entry.tile.meshe.geometry;
		let i, j, faceVerticesOffset;
		const geoVerticesNb = geo.attributes.position.array.length / 3;
		const bufferFaces = new Uint32Array(geo.index.array.length * this.nbLayers);
		const bufferVertices = new Float32Array(geo.attributes.position.array.length * this.nbLayers);
		const bufferUvs = new Float32Array(geo.attributes.uv.array.length * this.nbLayers);
		const bufferLayer = new Float32Array(geoVerticesNb * this.nbLayers);
		faceVerticesOffset = 0;
		for (i = 0; i < this.nbLayers; i ++) {
			for (j = 0; j < geoVerticesNb; j ++) {
				bufferLayer[j + faceVerticesOffset] = i + 1;
			}
			faceVerticesOffset += geoVerticesNb;
		}
		// Faces
		faceVerticesOffset = 0;
		let faceVerticesIndex = 0;
		for (i = 0; i < this.nbLayers; i ++) {
			for (j = 0; j < geo.index.array.length; j ++) {
				bufferFaces[j + faceVerticesOffset] = geo.index.array[j] + faceVerticesIndex;
			}
			faceVerticesOffset += geo.index.array.length;
			faceVerticesIndex += geoVerticesNb;
		}
		// UVs
		faceVerticesOffset = 0;
		for (i = 0; i < this.nbLayers; i ++) {
			for (j = 0; j < geo.attributes.uv.array.length; j ++) {
				bufferUvs[j + faceVerticesOffset] = geo.attributes.uv.array[j];
			}
			faceVerticesOffset += geo.attributes.uv.array.length;
		}
		const normals = [];
		const positions = [];
		for (i = 0; i < geo.attributes.normal.array.length / 3; i ++) {
			let positionVec = new THREE.Vector3().fromBufferAttribute(geo.attributes.position, i);
			let normalVec = new THREE.Vector3().fromBufferAttribute(geo.attributes.normal, i);
			normalVec.multiplyScalar(this.layerThickness);
			normals.push(normalVec);
			positions.push(positionVec);
		}
		let verticeLayerIndex = 0;
		for (i = 0; i < this.nbLayers; i ++) {
			for (j = 0; j < positions.length; j ++) {
				let normalVec = normals[j];
				let positionVec = positions[j];
				bufferVertices[verticeLayerIndex + (j * 3)] = positionVec.x;
				bufferVertices[verticeLayerIndex + (j * 3) + 1] = positionVec.y;
				bufferVertices[verticeLayerIndex + (j * 3) + 2] = positionVec.z;
				positionVec.add(normalVec);
			}
			verticeLayerIndex += geo.attributes.position.array.length;
		}
		var bufferGeometry = new THREE.BufferGeometry();
		bufferGeometry.addAttribute('layer', new THREE.BufferAttribute(bufferLayer, 1));
		bufferGeometry.addAttribute('position', new THREE.BufferAttribute(bufferVertices, 3));
		bufferGeometry.addAttribute('uv', new THREE.BufferAttribute(bufferUvs, 2));
		bufferGeometry.setIndex(new THREE.BufferAttribute(bufferFaces, 1));
		bufferGeometry.computeVertexNormals();
		bufferGeometry.computeBoundingBox();
		bufferGeometry.computeBoundingSphere();
		const bufferMesh = new THREE.Mesh(bufferGeometry, _material);
		_entry.tile.meshe.add(bufferMesh);
		_entry.grassMeshe = bufferMesh;
		App.Renderer.mustRender();
	}
	
	onTileDispose(_tile) {
		const tileKey = this.tileKey(_tile);
		const entry = this.tilesProcessed.filter(e => e.key == tileKey).forEach(e => {
			e.grassMeshe.parent.remove(e.grassMeshe);
			e.grassMeshe.geometry.dispose();
			e.grassMeshe = null;
		});
		super.onTileDispose(_tile);
	}
	
	dispose() {
		super.dispose();
		App.Globe.evt.removeEventListener('TILE_CHANGE-UPDATE_VERTICES', this, this.onTileUpdateVertices);
		App.Globe.evt.removeEventListener('TILE_CHANGE-READY', this, this.onTileReady);
		this.tilesProcessed.forEach(entry => {
			entry.tile.meshe.remove(entry.grassMeshe);
		});
		this.grassMaterial.dispose();
	}
}




App.Provider.Landuse = function(_callback) {
	this.isLoading = false;
	this.callback = _callback;
	this.params = {};
	this.ajax = new XMLHttpRequest();
	this.ajax.onreadystatechange = (evt) => {
		if (this.ajax.readyState==4){
			this.onDataLoadSuccess(this.ajax.responseText);
		}
	};
}

App.Provider.Landuse.prototype = {
	load : function(_params) {
		this.params = _params;
		this.isLoading = true;
		const cached = App.Cache.get('LANDUSE-' + this.params.keyCache);
		if (cached) {
			this.datasReady(cached);
			return true;
		}
		const url = 'https://val.openearthview.net/libs/remoteImg.php?overpass_surface=1&zoom='+_params.z + '&tileX='+_params.x+'&tileY='+_params.y;
		this.ajax.open("GET", url, true);
		this.ajax.send();
	}, 
	
	onDataLoadSuccess : function(_data) {
		App.Cache.set('LANDUSE-' + this.params.keyCache, _data);
		this.datasReady(_data);
	}, 
	
	datasReady : function(_datas) {
		this.isLoading = false;
		this.callback(_datas, this.params);
	}, 
}