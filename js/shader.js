var App = (function(_app) {	
	var api = {};
	
	api.get = function(_shaderName) {
		return {
			vertex : document.getElementById('vertex-' + _shaderName).textContent, 
			fragment : document.getElementById('fragment-' + _shaderName).textContent, 
		};
	}
	
	_app.Shader = api;
	return _app;
})(App);