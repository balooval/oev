'use strict';

App.require('geoTools.js');
App.require('vendor/lineclip.js');

var App = (function(_app) {
	
	var api = {};
	
	api.init = function() {
		
	}
	
	api.construct = function(_datas, _bbox) {
		const ways = api.extractFromJson(JSON.parse(_datas), _bbox);
		// console.log(ways);
		const canvasSize = 256;
		const canvas = document.createElement('canvas');
		canvas.width = canvasSize;
		canvas.height = canvasSize;
		const context = canvas.getContext('2d');
		context.fillStyle = 'rgba(0,0,0,0.0)';
		context.fillRect(0,0,canvasSize,canvasSize);
		ways.filter(w => w.surface !== null).forEach(w => {
			api.drawSurfaceToCanvas(canvas, w, _bbox)
		});
		
		return canvas;
	}
	
	api.extractFromJson = function(_data, _bbox) {
		const surfaces = [];
		let nodes = _data.elements.filter(e => e.type == 'node');
		let ways = _data.elements.filter(e => e.type == 'way');
		ways.forEach(w => {
			w.surfaceType = getWayType(w);
		});
		ways = ways.filter(w => w.surfaceType !== undefined);
		return ways.map(w => {
			const nodesCoords = w.nodes.map(wayNode => {
				const nodeDatas = nodes.filter(n => n.id == wayNode).pop();
				return {
					lon : nodeDatas.lon, 
					lat : nodeDatas.lat, 
				};
			});
			const surface = api.cutWay(nodesCoords, _bbox);
			return {
				type : w.surfaceType, 
				surface : surface, 
				nodesCoords : nodesCoords, 
			};
		});
	}
	
	api.cutWay = function(_nodesCoords, _bbox) {
		let flatCoords = _nodesCoords.map(n => [n.lon, n.lat]);
		const container = [_bbox.lon[0], _bbox.lat[1], _bbox.lon[1], _bbox.lat[0]];
		const patch = lineclip.polygon(flatCoords, container);
		if (patch.length == 0) return null;
		return patch;
	}
	
	api.drawSurfaceToCanvas = function(_canvas, _way, _bbox) {
		const context = _canvas.getContext('2d');
		const canvasPoints = _way.surface.map(point => [
			Math.round(Math.abs((((_bbox.lon[1] - point[0]) / _bbox.width) * _canvas.width) - _canvas.width)), 
			Math.round(((point[1] - _bbox.lat[0]) / _bbox.height) * _canvas.height), 
		]);
		const typesColors = {
			scrub : 150, 
			vineyard : 255, 
			wood : 80, 
			forest : 50, 
		};
		const typesHeight = {
			scrub : 100, 
			vineyard : 150, 
			wood : 245, 
			forest : 255, 
		};
		context.strokeStyle = 'rgba(0,0,0,0)';
		context.fillStyle = 'rgba(' + typesHeight[_way.type] + ',0,' + typesColors[_way.type] + ',1)';
		context.beginPath();
		const startPoint = canvasPoints.unshift();
		context.moveTo(startPoint[0], startPoint[1]);
		canvasPoints.forEach(point => context.lineTo(point[0], point[1]));
		context.closePath();
		context.fill();
		// context.stroke();
		return _canvas;
	}
	
	function getWayType(_way) {
		if (_way.tags.landuse) {
			if (_way.tags.landuse == 'vineyard') return 'vineyard';
			if (_way.tags.landuse == 'forest') return 'forest';
		}
		if (_way.tags.natural) {
			if (_way.tags.natural == 'wood') return 'wood';
			if (_way.tags.natural == 'scrub') return 'scrub';
		}
		return undefined;
	}
	
	_app.SurfaceMaker = api;
	
	return _app;
})(App);