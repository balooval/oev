'use strict';

App.require('tile.js');
App.require('geoTools.js');
App.require('sky.js');

var App = (function(_app) {
	let globalScale = 1;
	let meterInPos = 1;
	let tiles = [];
	let debugCoord = [270, 10];
	let decorators = [];
	
	var api = {};
	
	api.meshe;
	api.margin = 2;
	api.earthRadius = 6378137;
	api.radius = 1000000;
	api.evt = null;
	
	api.init = function() {
		api.evt = new Evt();
		api.updateMeter();
	}
	
	api.applyCamera = function(_geoCamera) {
		// api.displayZoomAtCoord(_geoCamera.zoom, _geoCamera.coord, 2);
		api.displayZoomAtCoord(_geoCamera.zoom, _geoCamera.groundCoord, 2);
	}
	
	api.displayZoomAtCoord = function(_zoom, _coord, _margin) {
		tiles.forEach(t => t.displayZoomAtCoord(_zoom, _coord, _margin));
	}, 
	
	api.construct = function(_zoom) {
		api.meshe = new THREE.Mesh(new THREE.Geometry());
		App.Renderer.add(api.meshe);
		const tileDefinition = 16;
		// const tileDefinition = 1;
		let tileMeshe;
		let tile;
		var nbTiles = Math.pow(2, _zoom);
		for(var curTileY = 1; curTileY <= nbTiles; curTileY ++){
			for(var curTileX = 1; curTileX <= nbTiles; curTileX ++){
				tile = new Tile(api, curTileX - 1, curTileY - 1, _zoom);
				tile.evt.listen('CHANGE', api, onTileChange);
				tile.buildMeshe(tileDefinition);
				tile.isReady();
				tile.show();
				tiles.push(tile);
			}
		}
		App.Sky.build();
		App.Renderer.mustRender();
	}
	
	function onTileChange(_evt) {
		api.evt.fireEvent('TILE_CHANGE-' + _evt.event, _evt.tile);
	}
	
	api.addDecorators = function(_decorator) {
		const decorator = App.DecoratorRegister.invoke(_decorator);
		decorators.push(decorator);
	}
	
	api.clearDecorators = function() {
		decorators.forEach(d => d.dispose());
		decorators = [];
	}
	
	api.circumference = function() {
		return Math.PI * 2 * api.earthRadius;
	}
	
	api.coordToVect = function(_coord) {
		const elevation = (_coord.ele * meterInPos) + api.radius;
		const radianX = App.Math.radians((_coord.lon - 180) * -1);
		const radianY = App.Math.radians(_coord.lat * -1);
		return new THREE.Vector3(
			Math.cos(radianX) * (elevation * Math.cos(radianY)), 
			Math.sin(radianY) * (elevation * -1), 
			Math.sin(radianX) * (elevation * Math.cos(radianY))
		);
	}
	
	api.vectToCoord = function(_vect) {
		let lon = 0;
		let lat = 0;
		const radianY = Math.asin(_vect.y / (api.radius * -1));
		lat = App.Math.degres(radianY) * -1;
		let angle = Math.atan2(_vect.z, _vect.x);
		lon = App.Math.degres(angle) * -1 - 180;
		if (lon < -180) lon += 360;
		return new App.GeoTools.Coord(lon, lat, 0);
	}
	
	api.localToPos = function(_localPos, _coordStart) {
		const coord = api.localToCoord(_localPos, _coordStart);
		return api.coordToVect(coord);
	}
	
	api.localToCoord = function(_localPos, _coordStart) {
		const fullCircumEarth = api.circumference();
		const quartCircumEarth = fullCircumEarth / 4;
		let startY = (_coordStart.lat / 90) * quartCircumEarth;
		const prctLat = (startY + _localPos.y) / quartCircumEarth;
		const finalLat = 90 * prctLat;
		let startX = (_coordStart.lon / 360) * fullCircumEarth;
		startX *= Math.cos(prctLat * (Math.PI / 2));
		const finalPosX = startX + _localPos.x;
		const circumAtLat = Math.cos(prctLat * (Math.PI / 2)) * fullCircumEarth;
		const prctLon = finalPosX / circumAtLat;
		const finalLon = (360 * prctLon);
		return new App.GeoTools.Coord(
			App.GeoTools.loopLon(finalLon), 
			finalLat, 
			_coordStart.ele + _localPos.z
		);
	}
	
	api.localToCoordOk = function(_localPos, _coordStart) {
		const dLon = _localPos.x / (api.earthRadius * Math.cos(App.Math.radians(_coordStart.lat)));
		const dLat = (_localPos.y / api.earthRadius);
		return new App.GeoTools.Coord(
			App.GeoTools.loopLon(_coordStart.lon + App.Math.degres(dLon)), 
			_coordStart.lat + App.Math.degres(dLat), 
			_coordStart.ele + _localPos.z
		);
	}
	
	api.elevationToZoom = function(_elevation) {
		const alt = ((api.earthRadius * globalScale) * Math.PI * 2) / _elevation;
		const zoom = Math.min(Math.max(Math.log(alt) / Math.log(2), 2), 18);
		return zoom;
	}
	

	api.zoomToElevation = function(_zoom, _lat){
		return api.earthRadius + (api.circumference() * Math.cos(_lat) / Math.pow(2, _zoom));
	}
		
	api.coordsElevation = function(_lon, _lat) {
		const coord = new App.GeoTools.Coord(_lon, _lat);
		const alt = tiles.map(t => t.elevationAt(coord)).filter(a => a !== false).pop();
		return alt;
	}
	
	api.toMeter = function(_distance) {
		return _distance * meterInPos;
	}
	
	api.updateMeter = function() {
		meterInPos = (api.radius / api.earthRadius) * globalScale;
	}
	
	_app.initOnStart(api);
	_app.Globe = api;
	
	return _app;
})(App);

