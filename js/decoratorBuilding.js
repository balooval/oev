'use strict';

App.require('decorator.js');
App.require('buildingMaker.js');

App.Decorator.Building = class extends App.Decorator {
	constructor() {
		super()
		this.name = 'BUILDING';
		const loadersNb = 1;
		for (let i = 0; i < loadersNb; i ++) {
			const loader = new App.Provider.Building((_data, _params) => this.onDatasLoaded(_data, _params));
			this.dataLoaders.push(loader);
		}
		App.Globe.evt.listen('TILE_CHANGE-READY', this, this.onTileReady);
		App.Globe.evt.listen('TILE_CHANGE-UPDATE_VERTICES', this, this.onTileUpdateVertices);
		App.DecoratorRegister.register(this);
	}
	
	onTileUpdateVertices(_tile) {
		const entry = this.tilesProcessed.filter(entry => entry.tile == _tile).pop();
		if (!entry) return false;
		// console.log('Tile with building has updated vertices');
		// entry.buildingsMeshe = _datas;
	}
	
	onTileReady(_tile) {
		if (_tile.zoom < 15) return false;
		const params = App.GeoTools.tilePosAtZoom(
			_tile.tileX, 
			_tile.tileY, 
			_tile.zoom, 
			15
		);
		params.keyCache = params.x + '-' + params.y + '-' + params.z;
		if (!this.addToWaiting(_tile, params)) return false;
		this.prioritizeQueue();
		this.processNextLoading();
	}
	
	onTileDispose(_tile) {
		const tileKey = this.tileKey(_tile);
		const entry = this.tilesProcessed.filter(e => e.key == tileKey).forEach(e => {
			e.buildingsMeshe.geometry.dispose();
			e.buildingsMeshe.material.dispose();
			e.buildingsMeshe = null;
		});
		super.onTileDispose(_tile);
	}
	
	onDatasLoaded(_datas, _params) {
		const entry = super.onDatasLoaded(_datas, _params);
		if (!entry) return false;
		if (!entry.tile.meshe) debugger;
		App.BuildingMaker.launchConstruction(_datas, entry.tile.bbox, entry.tile.zoom, meshe => {
			entry.buildingsMeshe = meshe;
			if (entry.tile.disposed) return false;
			entry.tile.meshe.add(entry.buildingsMeshe);
			App.Renderer.mustRender();
			this.onDataProcessed(entry);
		})
		// entry.buildingsMeshe = App.BuildingMaker.construct(_datas, entry.tile.bbox);
		// entry.tile.meshe.add(entry.buildingsMeshe);
		// App.Renderer.mustRender();
		// this.onDataProcessed(entry);
	}
	
	dispose() {
		super.dispose();
		App.Globe.evt.removeEventListener('TILE_CHANGE-READY', this, this.onTileReady);
		App.Globe.evt.removeEventListener('TILE_CHANGE-UPDATE_VERTICES', this, this.onTileUpdateVertices);
		this.tilesProcessed.forEach(entry => {
			entry.tile.meshe.remove(entry.buildingsMeshe);
		});
	}
}


App.Provider.Building = function(_callback) {
	this.isLoading = false;
	this.callback = _callback;
	this.params = {};
	this.ajax = new XMLHttpRequest();
	this.ajax.onreadystatechange = (evt) => {
		if (this.ajax.readyState==4){
			this.onDataLoadSuccess(this.ajax.responseText);
		}
	};
}

App.Provider.Building.prototype = {
	load : function(_params) {
		this.params = _params;
		this.isLoading = true;
		const cached = App.Cache.get('BUILDING-' + this.params.keyCache);
		if (cached) {
			this.datasReady(cached);
			return true;
		}
		const url = 'https://val.openearthview.net/libs/remoteImg.php?overpass_buildings=1&zoom='+_params.z+'&tileX='+_params.x+'&tileY='+_params.y
		this.ajax.open("GET", url, true);
		this.ajax.send();
	}, 
	
	onDataLoadSuccess : function(_data) {
		const processedDatas = App.BuildingMaker.prepare(_data, this.params.tile.bbox);
		App.Cache.set('BUILDING-' + this.params.keyCache, processedDatas);
		this.datasReady(processedDatas);
	}, 
	
	datasReady : function(_datas) {
		this.isLoading = false;
		this.callback(_datas, this.params);
	}, 
}