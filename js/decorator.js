'use strict';

App.require('cache.js');

App.Provider = {};

App.DecoratorRegister = (function() {
	const instances = {};
	const api = {};
	
	api.get = function(_decoratorName) {
		return instances[_decoratorName];
	}
	
	api.invoke = function(_decoratorName) {
		if (instances[_decoratorName]) return instances[_decoratorName];
		const instance = new App.Decorator[_decoratorName]();
		api.register(instance);
		return instance;
	}
	
	api.register = function(_instance) {
		instances[_instance.name] = _instance;
	}
	
	api.remove = function(_instance) {
		delete instances[_instance.name];
	}
	
	return api;
})();

App.Decorator = class {
	constructor() {
		this.name = 'DEFAULT';
		this.tilesWaitings = [];
		this.tilesProcessed = [];
		this.dataLoaders = [];
		App.Globe.evt.listen('TILE_CHANGE-DISPOSE', this, this.onTileDispose);
	}
	
	addToWaiting(_tile, _params) {
		const key = this.tileKey(_tile);
		const entry = this.tilesWaitings.filter(entry => entry.key == key).pop();
		if (entry) return false;
		this.tilesWaitings.push({
			key : key, 
			tile : _tile, 
			params : _params, 
			status : 'waiting', 
		});
		return true;
	}
	
	onDatasLoaded(_datas, _params) {
		const entry = this.tilesWaitings.filter(entry => entry.key == _params.key).pop();
		return entry;
	}
	
	onDataProcessed(_entry) {
		this.tilesProcessed.push(_entry);
		this.removeWaitingTile(_entry.key);
		// console.log(this.name, ': still', this.tilesWaitings.length, 'to load');
		this.processNextLoading();
	}
	
	onTileDispose(_tile) {
		this.removeWaitingTile(this.tileKey(_tile));
		this.removeProcessedTile(this.tileKey(_tile));
	}
	
	tileScore(_tile) {
		let score = _tile.zoom;
		score += _tile.visible ? 100 : 0;
		const distanceToCamera = Math.abs(_tile.bbox.center().lon - App.GeoCamera.coord.lon) + Math.abs(_tile.bbox.center().lat - App.GeoCamera.coord.lat);
		score -= distanceToCamera;		
		return score;
	}
	
	prioritizeQueue() {
		this.tilesWaitings.sort((a, b) => (this.tileScore(a.tile) > this.tileScore(b.tile)) ? 1 : -1);
	}
	
	tileKey(_tile) {
		return _tile.tileX + '-' + _tile.tileY + '-' + _tile.zoom
	}
	
	processNextLoading() {
		const dataLoader = this.dataLoaders.filter(d => !d.isLoading).pop();
		if (!dataLoader) return false;
		const entry = this.tilesWaitings.filter(e => e.status == 'waiting').pop();
		if (!entry) return false;
		entry.status = 'loading';
		dataLoader.load({
			...entry.params, 
			key : entry.key, 
			tile : entry.tile, 
		});
	}
	
	removeWaitingTile(_key) {
		this.tilesWaitings = this.tilesWaitings.filter(e => e.key != _key);
	}
	
	removeProcessedTile(_key) {
		this.tilesProcessed = this.tilesProcessed.filter(e => e.key != _key);
	}
	
	dispose() {
		App.Globe.evt.removeEventListener('TILE_CHANGE-DISPOSE', this, this.onTileDispose);
		this.tilesWaitings = [];
		App.DecoratorRegister.remove(this);
	}
}

class DecoratorProvider {
	constructor(_callback) {
		this.isLoading = false;
		this.callback = _callback;
		this.params = {};
	}
	
	load(_params) {
		this.params = _params;
		this.isLoading = true;
	}
}

class FakeProvider extends DecoratorProvider {
	load(_params) {
		super.load(_params);
		this.isLoading = false;
		this.callback(this.params);
	}
}

class Tile2DProvider extends DecoratorProvider {
	constructor(_baseUrl, _callback) {
		super(_callback);
		this.baseUrl = _baseUrl;
		this.cacheId = 'TILE2D-' + App.Utils.hashCode(this.baseUrl) + '-';
		this.tileLoader = new THREE.TextureLoader();
	}
	
	load(_params) {
		super.load(_params);
		const cached = App.Cache.get(this.cacheId + this.params.key);
		if (cached) {
			this.onDataLoadSuccess(cached);
			return true;
		}
		var loader = this;
		this.tileLoader.load(
			loader.baseUrl + '&z='+_params.z+'&x='+_params.x+'&y='+_params.y, 
			// 'https://val.openearthview.net/assets/debug/' + _params.z + '.png', 
			(data) => {
				App.Cache.set(this.cacheId + _params.key, data);
				loader.onDataLoadSuccess(data);
			}, 
			xhr => {},
			() => loader.onDataLoadError()
		);
	}
	
	onDataLoadSuccess(_data) {
		this.isLoading = false;
		this.callback(_data, this.params);
	}
}
