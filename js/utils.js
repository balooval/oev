'use strict';

var App = (function(_app) {
	var api = {};
	
	api.hashCode = function(_str) {
		var hash = 0, i, chr, len;
		if (_str.length === 0) return hash;
		for (i = 0, len = _str.length; i < len; i++) {
		chr   = _str.charCodeAt(i);
		hash  = ((hash << 5) - hash) + chr;
		hash |= 0; // Convert to 32bit integer
		}
		return hash;
	}
	
	api.getPolygonCentroid = function(pts) {
		var first = pts[0], last = pts[pts.length-1];
		if (first[0] != last[0] || first[1] != last[1]) pts.push(first);
		var twicearea=0,
		lon=0, lat=0,
		nPts = pts.length,
		p1, p2, f;
		for (var i=0, j=nPts-1 ; i<nPts ; j=i++) {
			p1 = pts[i]; p2 = pts[j];
			f = p1[0]*p2[1] - p2[0]*p1[1];
			twicearea += f;          
			lon += (p1[0] + p2[0]) * f;
			lat += (p1[1] + p2[1]) * f;
		}
		f = twicearea * 3;
		return [lon/f, lat/f];
	}
	
	api.pointInPolygon = function(_polygon, _x, _y) {
			var angle = 0;
			var ptA;
			var ptB;
			var segNb = _polygon.length;
			_polygon.push(_polygon[0]);
			for(var i = 0; i < segNb; i++) {
				ptA = _polygon[i];
				ptB = _polygon[i+1];
				angle += App.Math.angle2D(ptA[0] - _x, ptA[1] - _y, ptB[0] - _x, ptB[1] - _y);
			}
			if( Math.abs( angle ) < Math.PI ){
				return false;
			}
			return true;
	}, 
	
	_app.Utils = api;
	
	return _app;
})(App);

App.Utils.StepProcessor = (function() {
	var api = {};
	const process = [];
	
	api.init = function() {
		App.evt.listen('START', api, api.onStart);
	}
	
	api.onStart = function() {
		App.Renderer.evt.listen('RENDER', api, api.onStep);
	}
	
	api.queue = function(_workFn, _callback) {
		process.push({
			work : _workFn, 
			cb : _callback
		});
	}
	
	api.onStep = function() {
		if (process.length == 0) return false;
		const proc = process.pop();
		if (proc.cb) {
			proc.cb(proc.work());
		} else {
			proc.work();
		}
	}
	
	App.initOnStart(api);
	
	return api;
})();

App.Utils.Random = class {
	constructor(_seed) {
		this.seed = _seed % 2147483647;
		if (this.seed <= 0) this.seed += 2147483646;
	}
	
	/**
	 * Returns a pseudo-random value between 1 and 2^32 - 2.
	 */
	next() {
		return this.seed = this.seed * 16807 % 2147483647;
	}
	
	/**
	* Returns a pseudo-random floating point number in range [0, 1).
	*/
	nextFloat() {
		return (this.next() - 1) / 2147483646;
	}
}




