'use strict';

var App = (function(_app) {
	let geoCamera = null;
	let hasChanged = true;
	let grabMove = false;
	let grabRotate = false;
	let camRotation = {
		x : 1.5, 
		y : 0.2, 
	};
	let mouseLastPos = {
		x : 0, 
		y : 0
	};
	
	let testCamRotation = 0;
	let testTargetRotation = 0;
	let testCamDistance = 0;
	let testTargetDistance = 10000;
	let testCamRadius = 10000;
	
	var api = {};
	
	api.init = function(_geoCamera) {
		geoCamera = _geoCamera;
		App.Input.Mouse.evt.listen('MOUSE_WHEEL', api, onMouseWheel);
		App.Input.Mouse.evt.listen('MOUSE_LEFT_DOWN', api, onMouseLeftDown);
		App.Input.Mouse.evt.listen('MOUSE_LEFT_UP', api, onMouseLeftUp);
		App.Input.Mouse.evt.listen('MOUSE_RIGHT_DOWN', api, onMouseRightDown);
		App.Input.Mouse.evt.listen('MOUSE_RIGHT_UP', api, () => grabRotate = false);
		App.Input.Keyboard.evt.listen('ON_KEY_DOWN_107', api, () => onMouseWheel(0.2));
		App.Input.Keyboard.evt.listen('ON_KEY_DOWN_109', api, () => onMouseWheel(-0.2));
		App.Renderer.evt.listen('RENDER', api, api.onFrame);
	}
	
	function onMouseWheel(_delta) {
		geoCamera.setDistance(geoCamera.distance * (1 - _delta));
		hasChanged = true;
	}
	
	function onMouseRightDown() {
		grabRotate = true
		const mouseCoord = App.screenToCoord(
			App.Input.Mouse.curMouseX, 
			App.Input.Mouse.curMouseY, 
			App.Globe.meshe
		);
		if (!mouseCoord) return false;
		console.log('');
		geoCamera.testRotationCoord.set(mouseCoord.lon, mouseCoord.lat);
		
		// console.log(geoCamera.testRotationCoord);
		
		
		testTargetRotation = geoCamera.testRotationCoord.angleTo(geoCamera.testTargetCoord) - camRotation.x + Math.PI;
		testCamDistance = geoCamera.testCamCoord.distanceTo(geoCamera.testRotationCoord);
		testTargetDistance = geoCamera.testTargetCoord.distanceTo(geoCamera.testRotationCoord);
		testCamRadius = geoCamera.testCamCoord.distanceTo(testTargetDistance);
		
		
		// console.log(geoCamera.testCamCoord);
		// console.log(geoCamera.testRotationCoord);
		testCamRotation = geoCamera.testRotationCoord.angleTo(geoCamera.testCamCoord);
		console.log('testCamRotation', testCamRotation);
		
		// const rad = Math.atan2(Math.sin(testCamRotation), Math.cos(testCamRotation));
		// console.log('Deg', testCamRotation);
		// console.log('Rad', rad);
		// console.log('Degres', App.Math.degres(testCamRotation));
		// console.log('Cos', Math.cos(testCamRotation));
		// console.log('Sin', Math.sin(testCamRotation));
		
		
		// var revertLon = Math.cos(testCamRotation) * testCamDistance;
		// var revertLat = Math.sin(testCamRotation) * testCamDistance;
		// console.log('revertLon', revertLon);
		// console.log('revertLat', revertLat);
		// const testCamLocal = {
			// x : revertLon * -1, 
			// y : revertLat * -1, 
			// z : 0, 
		// };
		// console.log(App.Globe.localToCoord(testCamLocal, geoCamera.testCamCoord));
		
		// testCamRotation -= camRotation.x;
		// console.log('camRotation.x', camRotation.x);
		// console.log('B testCamRotation', testCamRotation);
		// testCamRotation += Math.PI;
		// testCamRotation += Math.PI / 2;
		// console.log('C testCamRotation', testCamRotation);
	}
	
	function onMouseLeftDown() {
		const groundHit = App.Renderer.screenPositionHitObject(
			App.Input.Mouse.curMouseX, 
			App.Input.Mouse.curMouseY, 
			App.Globe.meshe
		);
		if (!groundHit) return false;
		grabMove = true;
	}
	
	function onMouseLeftUp() {
		grabMove = false;
	}
	
	api.onFrame = function() {
		if (grabMove) api.move();
		if (grabRotate) api.rotate();
		if (hasChanged) {
			updateCamera();
			hasChanged = false;
		}
		mouseLastPos.x = App.Input.Mouse.curMouseX;
		mouseLastPos.y = App.Input.Mouse.curMouseY;
	}
	
	api.move = function() {
		const camAngle = Math.atan2(
			(geoCamera.groundCoord.lat - geoCamera.coord.lat) * 2, 
			geoCamera.groundCoord.lon - geoCamera.coord.lon
		);
		const mouseMove = calcMouseMove();
		const moveFactor = Math.pow(2, App.Globe.elevationToZoom(geoCamera.distance)) * 2;
		const depX = mouseMove.x / moveFactor;
		const depY = mouseMove.y / moveFactor;
		geoCamera.groundCoord.lon += (depX * Math.sin(camAngle) * -1) + (depY * Math.cos(camAngle));
		geoCamera.groundCoord.lat += (depY * Math.sin(camAngle) * 1) + (depX * Math.cos(camAngle));
		geoCamera.groundCoord.ele = App.Globe.coordsElevation(geoCamera.groundCoord.lon, geoCamera.groundCoord.lat);
		hasChanged = true;
	}
	
	api.rotate = function() {
		const mouseMove = calcMouseMove();
		camRotation.x += mouseMove.x / 150;
		camRotation.y += mouseMove.y / 150;
		if (camRotation.x > Math.PI) {
			camRotation.x -= Math.PI * 2;
		} else if (camRotation.x < -Math.PI) {
			camRotation.x += Math.PI * 2;
		}
		camRotation.y = Math.min(Math.max(camRotation.y, 0.05), Math.PI / 2);
		hasChanged = true;
	}
	
	function updateCamera() {
		const ele =  Math.cos(camRotation.y) * geoCamera.distance;
		const ratio = Math.sin(camRotation.y);
		const radius = ratio * geoCamera.distance;
		const localPos = {
			x : Math.cos(camRotation.x) * radius, 
			y : Math.sin(camRotation.x) * radius, 
			z : Math.max(ele + App.Globe.coordsElevation(geoCamera.coord.lon, geoCamera.coord.lat)), 
		};
		geoCamera.coord = App.Globe.localToCoord(localPos, geoCamera.groundCoord);
		
		const testCamLocal = {
			x : Math.sin(testCamRotation) * testCamDistance, 
			y : Math.cos(testCamRotation) * testCamDistance, 
			// z : Math.max(ele + App.Globe.coordsElevation(geoCamera.coord.lon, geoCamera.coord.lat)), 
			z : 0, 
		};
		geoCamera.testCamCoord = App.Globe.localToCoord(testCamLocal, geoCamera.testRotationCoord);
		
		const testTargetLocal = {
			x : Math.cos(testTargetRotation + camRotation.x) * (testTargetDistance * 1), 
			y : Math.sin(testTargetRotation + camRotation.x) * (testTargetDistance * 1), 
			z : 0, 
		};
		geoCamera.testTargetCoord = App.Globe.localToCoord(testTargetLocal, geoCamera.testRotationCoord);
		
		geoCamera.update();
		App.Renderer.mustRender();
	}
	
	function calcMouseMove() {
		return {
			x : App.Input.Mouse.curMouseX - mouseLastPos.x, 
			y : App.Input.Mouse.curMouseY - mouseLastPos.y, 
		};
	}
	
	_app.CameraControl = api;
	
	return _app;
})(App);