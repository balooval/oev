'use strict';

var App = (function(_app) {
	var api = {};
	
	api.radians = function(_degres){
		return Math.PI * _degres / 180;
	}
	
	api.degres = function(_radian){
		return _radian * 180 / Math.PI;
	}
	
	api.interpolate = function(_xA, _xB, _yA, _yB, _prctX, _prctY) {
		const diffYA = _xB - _xA;
		const diffYB = _yB - _yA;
		const interpolYA = _xA + diffYA * _prctX;
		const interpolYB = _yA + diffYB * _prctX;
		const diffY = interpolYB - interpolYA;
		const result = interpolYA + diffY * _prctY;
		return result;
	}
	
	api.angle2D = function(x1, y1, x2, y2) {
		var dtheta,theta1,theta2;
		theta1 = Math.atan2(y1, x1);
		theta2 = Math.atan2(y2, x2);
		dtheta = theta2 - theta1;
		while (dtheta > Math.PI){
			dtheta -= (Math.PI * 2);
		}
		while (dtheta < -Math.PI) {
			dtheta += (Math.PI * 2);
		}
		return dtheta;
	}
	
	_app.Math = api;
	
	return _app;
})(App);
