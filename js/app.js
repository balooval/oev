/*
* 
* Bootstrap
* Generic start point in charge of loading and updating application.
* 
*/
'use strict';

var App = (function(api){
    var appSrc = null;
    var required = [];
    var modulesToInit = [];
    var scriptsToLoad = [];
    var loadingScript = null;
    var subModules = [
        'vendor/three.min', 
        'vendor/simplify', 
        'vendor/seed-random', 
        'utils', 
        'event', 
        'input', 
        'renderer', 
        'globe', 
        'geoCamera', 
        'cameraControl', 
        'netTextures', 
        'netModels', 
        'assets', 
        'decorator', 
        'decoratorTexture', 
        'decoratorElevation', 
        'decoratorBuilding', 
        'decoratorNormal', 
        'decoratorGrass', 
        'decoratorTest', 
        'decoratorSea', 
        'decoratorWhales', 
    ];
    
    api.app = null;
    api.api = null;
    api.evt = null;
    
    api.init = function(_script) {
		api.require(_script);
        subModules.forEach(module => api.require(module + '.js'));
    }
    
    api.start = function() {
        console.log('App start');
        api.evt = new Evt();
		modulesToInit.forEach(m => m.init());
		App.Renderer.init('renderer-container');
		api.evt.fireEvent('START');
		App.Net.Model.loadBatch(App.Assets.Models, onModelsLoaded);
		
		// var coord = new App.GeoTools.Coord(4, 45);
		// var dep = {
			// x : 5000, 
			// y : 0, 
			// z : 0, 
		// };
		// var test = App.Globe.localToCoordTest(dep, coord);
		// console.log(test);
    }
	
	function onModelsLoaded() {
		App.Net.Texture.loadBatch(App.Assets.Textures, onRessourcesLoaded);
	}
	
	function onRessourcesLoaded() {
		console.log('onRessourcesLoaded');
		App.Globe.construct(4);
		App.Globe.addDecorators('Texture');
		// App.Globe.addDecorators('Elevation');
		// App.Globe.addDecorators('Sea');
		// App.Globe.addDecorators('Whales');
		// App.Globe.addDecorators('Building');
		// App.Globe.addDecorators('Normal');
		// App.Globe.addDecorators('Grass');
		App.Globe.addDecorators('Test');
		App.CameraControl.init(App.GeoCamera);
		App.Renderer.evt.listen('RENDER', api, api.onFrame);
	}
	
	api.onFrame = function() {
		App.Globe.applyCamera(App.GeoCamera);
		App.GeoCamera.updateRenderCamera(App.Renderer.camera);
	}
	
	api.screenToCoord = function(_x, _y, _object = undefined) {
		const vect = App.Renderer.screenPositionHitObject(_x, _y, _object);
		if (!vect) return null;
		return App.Globe.vectToCoord(vect.point);
	}
    
    api.initOnStart = function(_module) {
		modulesToInit.push(_module);
	}
	
    api.require = function(_srcFile) {
		if (!_srcFile) return false;
        if (required.indexOf(_srcFile) >= 0) return false;
		required.push(_srcFile);
		scriptsToLoad.push(_srcFile);
		if (!loadingScript) _loadSrc();
    }
	
    function _loadSrc() {
        if (scriptsToLoad.length == 0) {
            api.start();
			return false;
		}
		var nextScript = scriptsToLoad.shift();
		injectScript(nextScript);
    }
	
    function injectScript(_scriptName) {
		loadingScript = _scriptName;
        var script = document.createElement('script');
        script.setAttribute('id', 'script_' + _scriptName);
        script.type = 'text/javascript';
        script.async = false;
        script.onload = () => {
			loadingScript = null;
			_loadSrc();
		};
        script.addEventListener('error', _err => {
			console.warn('Error loading script');
			loadingScript = null;
		}, false);
        script.src = 'js/' + _scriptName;
        document.getElementsByTagName('head')[0].appendChild(script);
    }
	    
    return api;
    
}(App || {}));