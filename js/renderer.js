'use strict';

var App = (function(_app){
	let scene;
    let myRenderer;
	let elmt;
	let frameId = 0;
	let cameraWidth = 100;
	let paused = false;
	let doRender = true;
	let raycaster = null;
	
	var api = {
		evt : null, 
		camera : null, 
		containerOffset : null, 
		sceneWidth : 100, 
		sceneHeight : 100, 

		init : function(_containerId) {
			api.evt = new Evt();
			myRenderer = new THREE.WebGLRenderer({
				antialias : true, 
				logarithmicDepthBuffer : true, 
			});
			myRenderer.setClearColor(0x000000);
			elmt = document.getElementById(_containerId);
			api.containerOffset = new THREE.Vector2(elmt.offsetLeft, elmt.offsetTop);
			elmt.appendChild(myRenderer.domElement);
			window.onresize = onResize;
			api.camera = new THREE.PerspectiveCamera(90, elmt.clientWidth / elmt.clientHeight, 0.1, 20000000);
			api.camera.lookAt(0, 0, 0);
			api.camera.position.x = 0;
			api.camera.position.y = 0;
			api.camera.position.z = 10000 * 1.3;
			raycaster = new THREE.Raycaster();
			raycaster.near = api.camera.near;
			raycaster.far = api.camera.far;
			scene = new THREE.Scene();
			scene.add(api.camera);
			// scene.fog = new THREE.Fog(0xc5d3ea, 100000, 1000000);
			onResize();
			api.play();
		}, 
		
		mustRender : function() {
			doRender = true;
		}, 
				
		play : function() {
			api.renderNextFrame();
		}, 
		
		add : function(_obj) {
			scene.add(_obj);
		}, 
		
		remove : function(_obj) {
			scene.remove(_obj);
		}, 
		
		getCurFrame : function() {
			return frameId;
		}, 
		
		renderNextFrame : function() {
			requestAnimationFrame(api.renderNextFrame);
			if (paused) return false;
			frameId ++;
			api.evt.fireEvent('RENDER', frameId);
			api.evt.fireEvent('RENDER_FRAME_' + frameId);
			if (doRender) {
				myRenderer.render(scene, api.camera);
				doRender = false;
			}
		}, 
		
		intersectVect : function(_pos, _object) {
			raycaster.setFromCamera(new THREE.Vector2(_pos.x, _pos.y), api.camera);
			const intersects = raycaster.intersectObjects(_object.children);
			return intersects.pop();
		}, 
		
		screenPositionHitObject : function(_x, _y, _rootObject = scene) {
			const position = api.screenToContainer(_x, _y);
			return api.intersectVect(position, _rootObject);
		}, 
		
		screenToContainer : function(_x, _y) {
			return {
				x : ((_x - api.containerOffset.x) / api.sceneWidth) * 2 - 1, 
				y : -((_y - api.containerOffset.y) / api.sceneHeight) * 2 + 1
			};
		}, 
		
		setFogDensity : function(_near, _far) {
			// scene.fog.near = _near;
			// scene.fog.far = _far;
		}, 
		
		setFogColor : function(_color) {
			// scene.fog.color = _color;
		}, 
		
	};
	
	
	function onResize() {
		myRenderer.setSize(elmt.clientWidth, elmt.clientHeight);
		const ratio = elmt.clientWidth / elmt.clientHeight;
		api.camera.aspect = ratio;
		api.camera.updateProjectionMatrix();
		api.sceneWidth = elmt.clientWidth;
		api.sceneHeight = elmt.clientHeight;
	};
	
	_app.Renderer = api;
	
	return _app;
}(App));