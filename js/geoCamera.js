'use strict';

var App = (function(_app) {
	
	let debugPos = null;
	let debugGround = null;
	let debugTarget = null;
	let debugTestCam = null;
	let debugRotation = null;
	
	var api = {};
	api.zoom = 1;
	api.altitude = 0;
	api.coord = null;
	api.groundCoord = null;
	api.testCamCoord = null;
	api.testTargetCoord = null;
	api.testRotationCoord = null;
	api.evt = null;
	
	api.distance = 0;
	
	api.init = function() {
		api.evt = new Evt();
		App.evt.listen('START', api, api.onAppStart);
	}

	api.onAppStart = function() {
		// api.setAltitude(2000000);
		
		 // AV
		const startLon = 4.18221;
		const startLat = 43.73890;
		// St Hippo
		// const startLon = 3.8581;
		// const startLat = 43.9647;
		
		
		api.setDistance(8000);
		api.setAltitude(8000);
		// api.setAltitude(680);
		// api.coord = new App.GeoTools.Coord(3.8808, 43.6111, api.altitude); // Montpellier
		// api.coord = new App.GeoTools.Coord(3.8581, 43.9647, api.altitude); // St Hippo
		api.coord = new App.GeoTools.Coord(startLon, startLat, api.altitude); // AV
		// api.coord = new App.GeoTools.Coord(2.2428, 48.8901, api.altitude); // La defense
		api.groundCoord = new App.GeoTools.Coord(api.coord.lon, api.coord.lat, 0);
		api.testTargetCoord = new App.GeoTools.Coord(startLon, startLat - 0.039, 0);
		api.testCamCoord = new App.GeoTools.Coord(startLon, startLat, api.altitude);
		api.testRotationCoord = new App.GeoTools.Coord(startLon, startLat, 0);
		let geometrySphereA = new THREE.SphereGeometry(100000, 8, 8);
		let geometrySphereB = new THREE.SphereGeometry(100000, 8, 8);
		debugPos = new THREE.Mesh(geometrySphereA, new THREE.MeshBasicMaterial({color:0x0000ff}));
		debugGround = new THREE.Mesh(geometrySphereB, new THREE.MeshBasicMaterial({color:0x00ff00}));
		debugTarget = new THREE.Mesh(geometrySphereB, new THREE.MeshBasicMaterial({color:0xffff00}));
		debugTestCam = new THREE.Mesh(geometrySphereB, new THREE.MeshBasicMaterial({color:0xff00ff}));
		debugRotation = new THREE.Mesh(geometrySphereB, new THREE.MeshBasicMaterial({color:0xff0000}));
		App.Renderer.add(debugPos);
		App.Renderer.add(debugGround);
		App.Renderer.add(debugTarget);
		App.Renderer.add(debugTestCam);
		App.Renderer.add(debugRotation);
		api.updateDebug();
	}
	
	api.setDistance = function(_distance) {
		api.distance = _distance;
	}
	
	api.setAltitude = function(_altitude) {
		api.altitude = Math.max(_altitude, 10);
		
	}
	
	api.updateRenderCamera = function(_camera) {
		let vect = App.Globe.coordToVect(api.coord);
		// let vect = App.Globe.coordToVect(api.testCamCoord);
		_camera.position.set(vect.x, vect.y, vect.z);
		vect = App.Globe.coordToVect(api.groundCoord);
		// vect = App.Globe.coordToVect(api.testTargetCoord);
		_camera.lookAt(vect.x, vect.y, vect.z);
		_camera.up = vect.normalize();
	}
	
	api.update = function() {
		api.setZoom(App.Globe.elevationToZoom(this.coord.ele));
		api.updateDebug();
	}
	
	api.setZoom = function(_zoom) {
		if (isNaN(_zoom)) return false;
		const oldZoom = Math.floor(api.zoom);
		api.zoom = _zoom;	
		if (oldZoom != Math.floor(api.zoom)) {
			console.log('Zoom changed', Math.floor(api.zoom));
			api.evt.fireEvent('ZOOM_CHANGED', api.zoom);
		}
	}
	
	api.updateDebug = function() {
		let vect = App.Globe.coordToVect(api.groundCoord);
		debugGround.position.set(vect.x, vect.y, vect.z);
		const camCoordGround = new App.GeoTools.Coord(api.coord.lon, api.coord.lat, api.groundCoord.ele);
		vect = App.Globe.coordToVect(camCoordGround);
		debugPos.position.set(vect.x, vect.y, vect.z);
		vect = App.Globe.coordToVect(api.testTargetCoord);
		debugTarget.position.set(vect.x, vect.y, vect.z);
		vect = App.Globe.coordToVect(api.testRotationCoord);
		debugRotation.position.set(vect.x, vect.y, vect.z);
		
		vect = App.Globe.coordToVect(api.testCamCoord);
		debugTestCam.position.set(vect.x, vect.y, vect.z);
		const scale = 2 / Math.pow(2, api.zoom);
		debugGround.scale.set(scale, scale, scale);
		debugPos.scale.set(scale, scale, scale);
		debugTarget.scale.set(scale * 0.8, scale * 0.8, scale * 0.8);
		debugTestCam.scale.set(scale, scale, scale);
		debugRotation.scale.set(scale * 0.6, scale * 0.6, scale * 0.6);
		
		App.Renderer.mustRender();
	}
	
	_app.initOnStart(api);
	_app.GeoCamera = api;
	
	return _app;
})(App);