'use strict';

App.require('math.js');

var App = (function(_app) {
	
	var api = {};
	
	api.tilePosAtZoom = function(_tileX, _tileY, _zoom, _targetZoom) {
		const tilePos = {
			x : _tileX, 
			y : _tileY, 
			z : _targetZoom, 
		};
		for (var i = 0; i < (_zoom - _targetZoom); i ++) {
			let offsetX = tilePos.x % 2;
			let offsetY = tilePos.y % 2;
			tilePos.x = (tilePos.x - offsetX) / 2;
			tilePos.y = (tilePos.y - offsetY) / 2;
		}
		return tilePos;
	}
	
	api.tileToCoord = function(_tileX, _tileY, _zoom) {
		var p = [];
		var n = Math.PI - ((2.0 * Math.PI * _tileY) / Math.pow(2.0, _zoom));
		p[0] = ((_tileX / Math.pow(2.0, _zoom) * 360.0) - 180.0);
		p[1]= (180.0 / Math.PI * Math.atan(Math.sinh(n)));
		return p;
	}
	
	api.loopLon = function(_lon) {
		if (_lon > 180) _lon -= 360;
		if (_lon < -180) _lon += 360;
		return _lon
	}
	
	api.Coord = function(_lon = 0, _lat = 0, _ele = 0) {
		this.lon = _lon;
		this.lat = _lat;
		this.ele = _ele;
	}
	
	api.Coord.prototype = {
		set : function(_lon = 0, _lat = 0, _ele = 0) {
			this.lon = _lon;
			this.lat = _lat;
			this.ele = _ele;
		}, 
		
		distanceTo : function(_coord) {
			const dLat = App.Math.radians(_coord.lat - this.lat);
			const dLon = App.Math.radians(_coord.lon - this.lon); 
			var a = 
				Math.sin(dLat/2) * Math.sin(dLat/2) +
				Math.cos(App.Math.radians(this.lat)) * 
				Math.cos(App.Math.radians(_coord.lat)) * 
				Math.sin(dLon/2) * Math.sin(dLon/2); 
			const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
			const d = App.Globe.earthRadius * c;
			return d;
		}, 
		
		angleToOld : function(_coord) {
			const ratioA = Math.sin(App.Math.radians(this.lat));
			const ratioB = Math.sin(App.Math.radians(this.lat));
			return Math.atan2(this.lat - _coord.lat, this.lon * ratioA - _coord.lon * ratioB);
			
			// const quartRayonEarth = Math.PI * App.Globe.earthRadius / 2;
			// const startY = (this.lat / 90) * quartRayonEarth;
			// const prctLat = 1 - (startY / quartRayonEarth);
			// const startBY = (_coord.lat / 90) * quartRayonEarth;
			// const prctBLat = 1 - (startBY / quartRayonEarth);
// console.log(prctLat);
// console.log(prctBLat);
			
			// return Math.atan2(this.lat - _coord.lat, this.lon * prctLat - _coord.lon * prctBLat);
		}, 
		
		angleTo : function(_coord) {
			const dLon = ( _coord.lon - this.lon);
			const y = Math.sin(dLon) * Math.cos(_coord.lat);
			const x = Math.cos(this.lat) * Math.sin(_coord.lat) - Math.sin(this.lat) * Math.cos(_coord.lat) * Math.cos(dLon);
			return Math.atan2(y, x);
		}, 
	}
	
	api.BoundingBox = function(_startLon, _startLat, _endLon, _endLat) {
		this.start = [_startLon, _startLat];
		this.end = [_endLon, _endLat];
		this.lon = [_startLon, _endLon];
		this.lat = [_startLat, _endLat];
		this.width = _endLon - _startLon;
		this.height = _endLat - _startLat;
		this.startCoord = new App.GeoTools.Coord(_startLon, _startLat);
		this.endCoord = new App.GeoTools.Coord(_endLon, _endLat);
	}
	
	api.BoundingBox.prototype = {
		center : function() {
			return new App.GeoTools.Coord(
				this.endCoord.lon - this.width / 2, 
				this.endCoord.lat - this.height / 2, 
				0
			);
		}, 
		
		containsCoord : function(_coord) {
			if (this.startCoord.lon > _coord.lon) return false;
			if (this.startCoord.lat < _coord.lat) return false;
			if (this.endCoord.lon < _coord.lon) return false;
			if (this.endCoord.lat > _coord.lat) return false;
			return true;
		}, 
		
		containsBbox : function(_bbox) {
			if (this.start[0] > _bbox.end[0]) return false;
			if (this.start[1] > _bbox.end[1]) return false;
			if (this.end[0] < _bbox.start[0]) return false;
			if (this.end[1] < _bbox.start[1]) return false;
			return true;
		}, 
		
		scale : function(_scale) {
			const center = [this.start[0] + this.width / 2, this.start[1] + this.height / 2];
			const width = this.width * _scale;
			const height = this.height * _scale;
			const startLon = center[0] - width / 2;
			const startLat = center[1] - height / 2;
			const endLon = startLon + width;
			const endLat = startLat + height;
			return new App.GeoTools.BoundingBox(
				startLon, 
				startLat, 
				endLon, 
				endLat, 
			);
		}
	}
	
	_app.GeoTools = api;
	
	return _app;
})(App);

