'use strict';

App.require('decorator.js');
App.require('shader.js');

App.Decorator.Test = class extends App.Decorator {
	constructor() {
		super()
		this.name = 'TEST';
		// const loader = new FakeProvider(_params => this.onDatasLoaded(_params));
		const loader = new App.Provider.Landuse((_json, _params) => this.onDatasLoaded(_json, _params));
		this.dataLoaders.push(loader);
		this.testMaterial = this.prepareMaterial();
		App.Globe.evt.listen('TILE_CHANGE-READY', this, this.onTileReady);
		App.DecoratorRegister.register(this);
	}
	
	prepareMaterial() {
		const instanceShader = App.Shader.get('instance');
		return new THREE.ShaderMaterial( {
			vertexShader: instanceShader.vertex,
			fragmentShader: instanceShader.fragment, 
			// side: THREE.DoubleSide,
			transparent: true,
			depthTest: false, 
			depthWrite: true, 
			uniforms: {
				map : {type: 't', value: App.Net.Texture.get('tree')},
			}
		} );
	}
	
	onTileReady(_tile) {
		if (_tile.zoom < 15) return false;
		const params = App.GeoTools.tilePosAtZoom(
			_tile.tileX, 
			_tile.tileY, 
			_tile.zoom, 
			14
		);
		params.keyCache = params.x + '-' + params.y + '-' + params.z
		if (!this.addToWaiting(_tile, params)) return false;
		this.prioritizeQueue();
		this.processNextLoading();
	}
	
	onDatasLoaded(_json, _params) {
		const entry = super.onDatasLoaded(null, _params);
		if (!entry) return false;
		
		// let ways = App.SurfaceMaker.extractFromJson(JSON.parse(_json), entry.tile.bbox).filter(w => w.surface !== null);
		let canvas = App.SurfaceMaker.construct(_json, entry.tile.bbox);
		this.buildMeshe(entry, canvas);
		
		this.onDataProcessed(entry);
	}
	
	buildMeshe(_entry, _canvas) {
		var instances = 100;
		var geometry = new THREE.InstancedBufferGeometry();
		
		var pointGeo = new THREE.Geometry();
		
		var offsets = [];
		const hue = [];
		const angle = [];
		const heights = [];
		const orientations = [];
		const orientVect = new THREE.Vector3();
		const coord = new App.GeoTools.Coord();
		let posVect = new THREE.Vector3();
		
		const start = App.Globe.coordToVect(_entry.tile.bbox.startCoord)
		
		instances = Math.pow(4, (19 - _entry.tile.zoom)) * 5;
		const uvsBis = [];
		/*
		const def = (_entry.tile.zoom - 15) + 4;
		const uvStep = 1 / def;
		const angleStep = (Math.PI * 2) / def;
		const sections = 6;
		const uvY = [0, 0.15, 0.25, 0.5, 0.65, 0.80, 1.0];
		const radius = [3, 2, 2, 10, 18, 12, 1].map(r => r * 0.2);
		let vertPos = [];
		const altitudes = [0, 5, 12, 12, 18, 27, 32].map(r => r * 0.3);
		for (let s = 0; s < sections; s ++) {
			for (let a = 0; a < def; a ++) {
				const curHeight = altitudes[s];
				const nextHeight = altitudes[s + 1];
				const curAngle = angleStep * a;
				const nextAngle = angleStep * (a + 1);
				let face;
				face = [];
				face.push(Math.cos(curAngle) * radius[s]);
				face.push(Math.sin(curAngle) * radius[s]);
				face.push(curHeight);
				vertPos.push(face);
				uvsBis.push(a * uvStep);
				// uvsBis.push(curHeight / Math.max(...altitudes));
				uvsBis.push(uvY[s]);
				
				face = [];
				face.push(Math.cos(nextAngle) * radius[s]);
				face.push(Math.sin(nextAngle) * radius[s]);
				face.push(curHeight);
				vertPos.push(face);
				uvsBis.push((a + 1) * uvStep);
				// uvsBis.push(curHeight / Math.max(...altitudes));
				uvsBis.push(uvY[s]);
				
				face = [];
				face.push(Math.cos(curAngle) * radius[s + 1]);
				face.push(Math.sin(curAngle) * radius[s + 1]);
				face.push(nextHeight);
				vertPos.push(face);
				uvsBis.push((a + 0) * uvStep);
				// uvsBis.push(nextHeight / Math.max(...altitudes));
				uvsBis.push(uvY[s + 1]);
				
				face = [];
				face.push(Math.cos(nextAngle) * radius[s]);
				face.push(Math.sin(nextAngle) * radius[s]);
				face.push(curHeight);
				vertPos.push(face);
				uvsBis.push((a + 1) * uvStep);
				// uvsBis.push(curHeight / Math.max(...altitudes));
				uvsBis.push(uvY[s]);
				
				face = [];
				face.push(Math.cos(nextAngle) * radius[s + 1]);
				face.push(Math.sin(nextAngle) * radius[s + 1]);
				face.push(nextHeight);
				vertPos.push(face);
				uvsBis.push((a + 1) * uvStep);
				// uvsBis.push(nextHeight / Math.max(...altitudes));
				uvsBis.push(uvY[s+1]);
				
				face = [];
				face.push(Math.cos(curAngle) * radius[s + 1]);
				face.push(Math.sin(curAngle) * radius[s + 1]);
				face.push(nextHeight);
				vertPos.push(face);
				uvsBis.push((a + 0) * uvStep);
				// uvsBis.push(nextHeight / Math.max(...altitudes));
				uvsBis.push(uvY[s+1]);
			}
		}
		
		
		const verticesPositions = [];
		
		vertPos = vertPos.map(face => {
			face = face.map(pos => pos * 1);
			const pos = App.Globe.localToPos({x:face[0], y:face[1], z:face[2]}, _entry.tile.bbox.startCoord);
			pos.x -= start.x;
			pos.y -= start.y;
			pos.z -= start.z;
			verticesPositions.push(pos.x, pos.y, pos.z);
			return pos;
		});
		
		geometry.addAttribute('uv', new THREE.Float32BufferAttribute(uvsBis, 2));
		geometry.addAttribute('position', new THREE.Float32BufferAttribute(verticesPositions, 3));
		*/
		// const bufferGeometry = App.Net.Model.get('tree');
		// geometry.addAttribute('position', new THREE.Float32BufferAttribute(bufferGeometry.geometry.attributes.position.array, 3));
		
		
		const context = _canvas.getContext('2d');
		
		let instanceNb = 0;
		
		const pixelFromCoord = (_lon, _lat) => {
			return [
				Math.round(Math.abs(((( _entry.tile.bbox.lon[1] - _lon) /  _entry.tile.bbox.width) * _canvas.width) - _canvas.width)), 
				Math.round(((_lat -  _entry.tile.bbox.lat[0]) /  _entry.tile.bbox.height) * _canvas.height)
			];
		}
		
		
		const params = App.GeoTools.tilePosAtZoom(
			_entry.tile.tileX, 
			_entry.tile.tileY, 
			_entry.tile.zoom, 
			14
		);
		const randomGen = new alea(params.x + '' + params.y + '' + params.z);
		
		for (let i = 0; i < instances; i++ ) {
			const posLon = _entry.tile.bbox.startCoord.lon + (randomGen() * _entry.tile.bbox.width);
			const posLat = _entry.tile.bbox.startCoord.lat + (randomGen() * _entry.tile.bbox.height);
			const pxl = pixelFromCoord(posLon, posLat);
			var c = context.getImageData(pxl[0], pxl[1], 1, 1).data;
			if (c[0] == 0) continue;
			
			
			instanceNb ++;
			coord.set(
				posLon, 
				posLat, 
				App.Globe.coordsElevation(posLon, posLat) + 5, 
				// 0, 
			);
			posVect = App.Globe.coordToVect(coord);
			/*
			offsets.push(posVect.x, posVect.y, posVect.z);
			// orientVect.set(Math.random() * 3 - 1.5, Math.random() * 3 - 1.5, Math.random() * 3 - 1.5).normalize();
			orientVect.set(start.x, start.y, start.z).normalize();
			orientations.push(orientVect.x, orientVect.y, orientVect.z);
			angle.push(Math.random() * 2 - 1);
			// angle.push(0);
			let height = Math.random() + 0.8;
			if (Math.random() > 0.95) height *= Math.random() + 0.6;
			heights.push(height);
			hue.push(Math.random());
			*/
			pointGeo.vertices.push(new THREE.Vector3(posVect.x, posVect.y, posVect.z));
		}
		/*
		geometry.maxInstancedCount = instanceNb;
		geometry.addAttribute('offset', new THREE.InstancedBufferAttribute(new Float32Array(offsets), 3));
		geometry.addAttribute('angle', new THREE.InstancedBufferAttribute(new Float32Array(angle), 1));
		geometry.addAttribute('orientation', new THREE.InstancedBufferAttribute(new Float32Array(orientations), 3));
		geometry.addAttribute('height', new THREE.InstancedBufferAttribute(new Float32Array(heights), 1));
		geometry.addAttribute('hue', new THREE.InstancedBufferAttribute(new Float32Array(hue), 1));
		_entry.testMeshe = new THREE.Mesh(geometry, this.testMaterial);
		_entry.testMeshe.frustumCulled = false;
		_entry.tile.meshe.add(_entry.testMeshe);
		*/
		
		
		const shader = App.Shader.get('point');
		const pointMaterial = new THREE.PointsMaterial({color:0xffffff,size:App.Globe.toMeter(10),map:App.Net.Texture.get('pointTree'),transparent:true,alphaTest:0.5,});
		/*
		const pointMaterial = new THREE.ShaderMaterial( {
			vertexShader: shader.vertex,
			fragmentShader: shader.fragment, 
			side: THREE.DoubleSide,
			transparent: true,
			uniforms: {
				// map : {type: 't', value: App.Net.Texture.get('tree')},
			}
		} );
		*/
		var particles = new THREE.Points(pointGeo, pointMaterial);
		_entry.testMeshe = particles;
		_entry.tile.meshe.add(_entry.testMeshe);
		
		App.Renderer.mustRender();
	}
	
	onTileDispose(_tile) {
		const tileKey = this.tileKey(_tile);
		const entry = this.tilesProcessed.filter(e => e.key == tileKey).filter(e => e.testMeshe).forEach(e => {
			e.testMeshe.parent.remove(e.testMeshe);
			e.testMeshe.geometry.dispose();
			e.testMeshe = null;
		});
		super.onTileDispose(_tile);
	}
	
	dispose() {
		super.dispose();
		App.Globe.evt.removeEventListener('TILE_CHANGE-READY', this, this.onTileReady);
		this.tilesProcessed.forEach(entry => {
			entry.testMeshe.parent.remove(entry.testMeshe);
		});
		this.testMaterial.dispose();
	}
}




App.Provider.Landuse = function(_callback) {
	this.isLoading = false;
	this.callback = _callback;
	this.params = {};
	this.ajax = new XMLHttpRequest();
	this.ajax.onreadystatechange = (evt) => {
		if (this.ajax.readyState==4){
			this.onDataLoadSuccess(this.ajax.responseText);
		}
	};
}

App.Provider.Landuse.prototype = {
	load : function(_params) {
		this.params = _params;
		this.isLoading = true;
		const cached = App.Cache.get('LANDUSE-' + this.params.keyCache);
		if (cached) {
			this.datasReady(cached);
			return true;
		}
		const url = 'https://val.openearthview.net/libs/remoteImg.php?overpass_surface=1&zoom='+_params.z + '&tileX='+_params.x+'&tileY='+_params.y;
		this.ajax.open("GET", url, true);
		this.ajax.send();
	}, 
	
	onDataLoadSuccess : function(_data) {
		App.Cache.set('LANDUSE-' + this.params.keyCache, _data);
		this.datasReady(_data);
	}, 
	
	datasReady : function(_datas) {
		this.isLoading = false;
		this.callback(_datas, this.params);
	}, 
}