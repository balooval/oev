'use strict';

App.require('decorator.js');

App.Decorator.Elevation = class extends App.Decorator {
	constructor() {
		super()
		this.name = 'ELEVATION';
		const loadersNb = 4;
		for (let i = 0; i < loadersNb; i ++) {
			const loader = new App.Provider.Elevation((_data, _params) => this.onDatasLoaded(_data, _params));
			this.dataLoaders.push(loader);
		}
		App.Globe.evt.listen('TILE_CHANGE-READY', this, this.onTileReady);
		App.DecoratorRegister.register(this);
	}
	
	onTileReady(_tile) {
		const params = {
			x : _tile.tileX, 
			y : _tile.tileY, 
			z : _tile.zoom, 
		};
		if (!this.addToWaiting(_tile, params)) return false;
		this.prioritizeQueue();
		this.processNextLoading();
	}
	
	onDatasLoaded(_datas, _params) {
		const entry = super.onDatasLoaded(_datas, _params);
		if (!entry) return false;
		entry.tile.elevation = _datas;
		entry.tile.updateVerticesPos();
		this.onDataProcessed(entry);
	}
	
	dispose() {
		super.dispose();
		App.Globe.evt.removeEventListener('TILE_CHANGE-READY', this, this.onTileReady);
		this.tilesProcessed.forEach(entry => {
			entry.tile.elevation.fill(0);
			entry.tile.updateVerticesPos();
		});
	}
}


App.Provider.Elevation = function(_callback) {
	this.isLoading = false;
	this.callback = _callback;
	this.params = {};
	var loader = this;
	this.imageObj = new Image();
	this.imageObj.crossOrigin = 'Anonymous';
	this.imageObj.onload = function() {
		loader.onImageLoaded(this);
	};
}

App.Provider.Elevation.prototype = {
	load : function(_params) {
		this.params = _params;
		this.isLoading = true;
		const cached = App.Cache.get('ELEVATION-' + this.params.key);
		if (cached) {
			this.dataReady(cached);
			return true;
		}
		this.imageObj.src = 'https://val.openearthview.net/libs/remoteImg.php?tileEle=1&def=' + _params.tile.tileDefinition + '&z='+_params.z+'&x='+_params.x+'&y='+_params.y;
	}, 
	
	onImageLoaded : function(_img) {
		const res = App.Provider.Canvas.extractElevation(_img, _img.width, _img.height);
		App.Cache.set('ELEVATION-' + this.params.key, res);
		this.dataReady(res);
	}, 
	
	dataReady : function(_data) {
		this.isLoading = false;
		if (this.callback) {
			this.callback(_data, this.params);
		}
	}
}


App.Provider.Canvas = (function() {
	var canvas = document.createElement('canvas');
	canvas.width = '64';
	canvas.height = '64';
	var context = canvas.getContext('2d');
	
	var api = {
		extractElevation : function(_img, _w, _h) {
			context.drawImage(_img, 0, 0, _w, _h);
			var img = context.getImageData(0, 0, _w, _h); 
			var imgWidth = _w;
			var imgHeight = _h;
			var imageData = context.getImageData(0, 0, imgWidth, imgHeight);
			var data = imageData.data;
			var eleBuffer = new Uint16Array(data.length / 4);
			var x, y;
			var index;
			var red;
			var green;
			var blue;
			var alt;
			var bufferIndex = 0;
			for (x = 0; x < imgWidth; ++x) {
				for (y = 0; y < imgHeight; ++y) {
					index = (y * imgWidth + x) * 4;
					red = data[index];
					green = data[++index];
					blue = data[++index];
					alt = red * 256 + blue;
					eleBuffer[bufferIndex] = alt;
					bufferIndex ++;
				}
			}
			return eleBuffer;
		}
	};
	
	return api;
})();