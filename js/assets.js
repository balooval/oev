'use strict';

var App = (function(_app){
	_app.Assets = {};
	
	var serverPath = 'https://val.openearthview.net/';
	var texturesPath = serverPath + 'assets/textures/';
	var modelsPath = serverPath + 'assets/models/';
	
	_app.Assets.Textures = [
		{
			id : 'loading', 
			url : texturesPath + 'loading.png', 
		}, 
		{
			id : 'skyColors', 
			url : texturesPath + 'sky_gradient.png', 
		}, 
		{
			id : 'sun', 
			url : texturesPath + 'sun.png', 
		}, 
		{
			id : 'fur', 
			url : texturesPath + 'fur.png', 
		}, 
		{
			id : 'grass', 
			url : texturesPath + 'instance-grass.jpg', 
		}, 
		{
			id : 'tree', 
			url : texturesPath + 'tree-instance.png',
		}, 
		{
			id : 'pointTree', 
			url : texturesPath + 'natural_tree.png',
		},
		{
			id : 'coastLine', 
			url : serverPath + 'libs/remoteImg.php?coastLine',
		}, 
	];
	
	_app.Assets.Models = [
		{
			id : 'tree', 
			url : modelsPath + 'tree_lod_2.json', 
		}, 
		{
			id : 'whale', 
			url : modelsPath + 'whale.json', 
		}, 
	];
	
	return _app;
})(App || {});