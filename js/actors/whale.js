class Whale {
	constructor(_bbox) {
		this.bbox = _bbox;
		const vectPos = App.Globe.coordToVect(this.bbox.center());
		this.mesh = this.buildMeshe();
		this.mesh.position.set(vectPos.x, vectPos.y, vectPos.z);
		const scale = 10;
		this.mesh.scale.set(scale, scale, scale);
		App.Renderer.add(this.mesh);
	}
	
	buildMeshe() {
		const bufferGeometry = App.Net.Model.get('whale').geometry;
		const material = new THREE.MeshBasicMaterial({color:0x9c9fa3});
		return new THREE.Mesh(bufferGeometry, material);
	}
	
	dispose() {
		App.Renderer.remove(this.mesh);
		this.mesh.geometry.dispose();
		this.mesh.material.dispose();
		this.mesh = null;
	}
}