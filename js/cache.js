'use strict';

var App = (function(_app) {
	var ressources = {};
	
	var api = {};
	
	api.set = function(_key, _data) {
		if (ressources[_key]) return false;
		ressources[_key] = _data;
	}
	
	api.get = function(_key) {
		if (!ressources[_key]) return null;
		return ressources[_key];
	}
	
	_app.Cache = api;
	
	return _app;
})(App);

