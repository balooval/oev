'use strict';

App.require('decorator.js');


App.Decorator.Sea = class extends App.Decorator {
	constructor() {
		super()
		this.name = 'SEA';
		this.coastDatasSize = 0;
		this.coastPixelRatio = 1;
		App.Globe.evt.listen('TILE_CHANGE-READY', this, this.onTileReady);
		this.coastDatas = this.prepareGroundDatas(App.Net.Texture.get('coastLine').image);
		App.DecoratorRegister.register(this);
	}
	
	onTileReady(_tile) {
		if (_tile.zoom < 9) return false;
		if (this.isCoordOnGround(_tile.bbox.center())) return false;
		// _tile.material.map = App.Net.Texture.get('fur');
		// _tile.material.needsUpdate = true;
		// App.Renderer.mustRender();
	}
	
	isCoordOnGround(_coord) {
		const mercX = this.mercatorLonToX(_coord.lon);
		const mercY = this.mercatorLatToY(_coord.lat);
		var pxlX = Math.round(mercX * this.coastPixelRatio) + (this.coastDatasSize / 2);
		let pxlY = Math.round(mercY * this.coastPixelRatio) + (this.coastDatasSize / 2);
		pxlY = Math.abs(this.coastDatasSize - pxlY);
		return this.coastDatas[this.bufferIndex(pxlX, pxlY)];
	}
		
	prepareGroundDatas(_img) {
		this.coastDatasSize = _img.width;
		this.coastPixelRatio = this.coastDatasSize / App.Globe.circumference();
		const canvas = document.createElement('canvas');
		canvas.width = this.coastDatasSize;
		canvas.height = this.coastDatasSize;
		const context = canvas.getContext('2d');
		context.drawImage(_img, 0, 0, this.coastDatasSize, this.coastDatasSize);
		const data = context.getImageData(0, 0, this.coastDatasSize, this.coastDatasSize).data;
		const coastDatas = new Int8Array(data.length / 4);
		let x, y, red;
		let bufferIndex = 0;
		for (x = 0; x < this.coastDatasSize; ++x) {
			for (y = 0; y < this.coastDatasSize; ++y) {
				red = data[this.pixelIndex(x, y)];
				coastDatas[bufferIndex] = red > 0 ? 1 : 0;
				bufferIndex ++;
			}
		}
		return coastDatas;
	}
	
	mercatorLonToX(_lon) {
		return 6378137 * App.Math.radians(_lon);
	}
	
	mercatorLatToY(_lat) {
		_lat = Math.min(Math.max(_lat, -89.5), 89.5);
		const r_major = 6378137.000;
		const r_minor = 6356752.3142;
		const temp = r_minor / r_major;
		const eccent = Math.sqrt(1.0 - (temp * temp));
		const phi = App.Math.radians(_lat);
		const com = 0.5 * eccent;
		let con = eccent * Math.sin(phi);
		con = Math.pow((1.0-con)/(1.0+con), com);
		const ts = Math.tan(.5 * (Math.PI*0.5 - phi))/con;
		return 0 - r_major * Math.log(ts);
	}
	
	bufferIndex(_x, _y) {
		return _x * this.coastDatasSize + _y;
	}
	
	pixelIndex(_x, _y) {
		return (_y * this.coastDatasSize + _x) * 4;
	}
	
	dispose() {
		super.dispose();
		App.Globe.evt.removeEventListener('TILE_CHANGE-READY', this, this.onTileReady);
	}
}